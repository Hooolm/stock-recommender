﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StcokRecommender.Services;
using StcokRecommender.Services.Models.Basic;
using StcokRecommender.Services.Models.Technical;
using StockRecommender.Models.Technical;

namespace StockRecommender.Backtester
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            string symbol = null;

            while (symbol != "STOP")
            {
                Console.WriteLine($"Stonk?");
                symbol = (Console.ReadLine()).ToUpper();
                var earliestSimBack = DateTime.Today.AddYears(-2);
                var simDate = DateTime.Today.AddMonths(-6);
                var today = DateTime.Today;
                var ts = await YahooService.GetTimeseriesAsync(symbol, "1d", earliestSimBack, today);
                var cashBalance = 100000m;
                var shares = 0;

                var naiveShares = 0;
                var naiveCashbalance = cashBalance;

                var gains = 0m;
                var naiveGains = 0m;

                while (simDate <= today)
                {
                    var simSeries = new TimeSeries()
                    {
                        Series = ts.Series.Where(x => x.Timestamp > earliestSimBack && x.Timestamp < simDate).ToList(),
                        Symbol = ts.Symbol,
                        Currency = ts.Currency,
                        Interval = ts.Interval
                    };

                    var currentPrice = simSeries.Series.Last(x => x.HasRegularPriceValues).Close.Value;

                    if (naiveShares == 0)
                    {
                        var possibleBuy = (int)Math.Floor(naiveCashbalance / currentPrice);
                        if (possibleBuy > 0)
                        {
                            naiveShares = naiveShares + possibleBuy;
                            naiveCashbalance = naiveCashbalance - possibleBuy * currentPrice;
                        }
                    }

                    var touchRanksTasks = new List<Task<TouchRank>>
                    {
                    };

                    var result = (await Task.WhenAll(touchRanksTasks));

                    var buySignals =
                        result.Count(x => x.Signal == Signal.Buy);
                    var sellSignals = result.Count(x =>
                        x.Signal == Signal.Sell);

                    var action = "HOLD";

                    if (buySignals > sellSignals)
                    {
                        var possibleBuy = (int)Math.Floor(cashBalance / currentPrice);
                        if (possibleBuy > 0)
                        {
                            action = "BUY ";
                            shares = shares + possibleBuy;
                            cashBalance = cashBalance - possibleBuy * currentPrice;
                        }
                    }
                    else if (buySignals < sellSignals)
                    {
                        if (shares > 0)
                        {
                            action = "SELL";
                        }

                        cashBalance = cashBalance + shares * currentPrice;
                        shares = 0;
                    }

                    gains = shares * currentPrice + cashBalance;
                    naiveGains = naiveShares * currentPrice + naiveCashbalance;

                    Console.WriteLine(
                        $"{simDate.ToShortDateString()} : {action} : {shares,6} @ {currentPrice,6:0.00} :  {gains,6:0} : {gains / naiveGains:0.00}");

                    earliestSimBack = earliestSimBack.AddDays(1);
                    simDate = simDate.AddDays(1);
                }

                Console.WriteLine($"{gains.ToString("0.##")} : {gains / naiveGains}");
            }
        }
    }
}