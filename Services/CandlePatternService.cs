﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StcokRecommender.Services.Models.Basic;
using StcokRecommender.Services.Models.Technical;

namespace StcokRecommender.Services
{
    public static class CandlePatternService
    {
        public static async Task<List<CandlePattern>> GetCandlePatternsAsync(string symbol, string interval,
            DateTime? from = null, DateTime? to = null)
        {
            return await GetCandlePatternsAsync(await YahooService.GetTimeseriesAsync(symbol, interval, from, to));
        }

        public static async Task<List<CandlePattern>> GetCandlePatternsAsync(TimeSeries timeSeries)
        {
            var sliceSize = 6;
            var series = timeSeries.Series.Where(x => x.HasRegularPriceValues).Select(x => new Candle(x)).ToList();
            var patterns = new List<CandlePattern>();
            for (int i = 0; i <= series.Count - sliceSize; i++)
            {
                var slice = series.Skip(i).Take(sliceSize).ToList();
                patterns.AddRange(await GetPatternIfExists(slice));
            }

            return patterns;
        }

        private static async Task<List<CandlePattern>> GetPatternIfExists(List<Candle> slice)
        {
            var patterns = new List<CandlePattern>();
            AddHammerPatternIfExists(slice, patterns);
            AddEngulfPatternIfExists(slice, patterns);
            AddMidCandleReversalPatternIfExists(slice, patterns);
            AddThreeCandleStairPatternIfExists(slice, patterns);
            return patterns;
        }

        private static void AddThreeCandleStairPatternIfExists(List<Candle> slice, List<CandlePattern> patterns)
        {
            var before = slice.Take(2);
            var stairSlice = slice.Skip(2).Take(3).ToList();

            if (stairSlice.All(ss =>
                    ss.Color == Quote.CandleColor.Green && before.All(bf => bf.Color == Quote.CandleColor.Red)))
            {
                var confirmed = true;
                for (int i = 1; i < stairSlice.Count; i++)
                {
                    var currentCandle = stairSlice[i];
                    var prevCandle = stairSlice[i - 1];
                    if (currentCandle.BarBottom < prevCandle.BarTop)
                    {
                        confirmed = false;
                        break;
                    }
                }

                if (confirmed)
                {
                    patterns.Add(new CandlePattern(slice, PatternType.WhiteSoldiers));
                }
            }

            if (stairSlice.All(ss =>
                    ss.Color == Quote.CandleColor.Red && before.All(bf => bf.Color == Quote.CandleColor.Green)))
            {
                var confirmed = true;
                for (int i = 1; i < stairSlice.Count; i++)
                {
                    var currentCandle = stairSlice[i];
                    var prevCandle = stairSlice[i - 1];
                    if (currentCandle.BarTop > prevCandle.BarBottom)
                    {
                        confirmed = false;
                        break;
                    }
                }

                if (confirmed)
                {
                    patterns.Add(new CandlePattern(slice, PatternType.BlackCrows));
                }
            }
        }

        private static void AddMidCandleReversalPatternIfExists(List<Candle> slice, List<CandlePattern> patterns)
        {
            var beforeCandles = slice.Take(2).ToList();
            var afterCandles = slice.Skip(4).Take(2).ToList();
            var firstCandle = slice[2];
            var secondCandle = slice[3];

            if (firstCandle.Color == Quote.CandleColor.Red && secondCandle.Color == Quote.CandleColor.Green
                                                           && firstCandle.BarBottom < secondCandle.BarMiddle
                                                           && beforeCandles.All(bf =>
                                                               bf.Color == Quote.CandleColor.Red &&
                                                               bf.BarBottom >= firstCandle.BarTop)
                                                           && afterCandles.All(aft =>
                                                               aft.Color == Quote.CandleColor.Green &&
                                                               aft.BarBottom >= secondCandle.BarTop))
            {
                patterns.Add(new CandlePattern(slice, PatternType.PiercingLine));
            }

            if (firstCandle.Color == Quote.CandleColor.Green && secondCandle.Color == Quote.CandleColor.Red
                                                             && secondCandle.BarBottom < firstCandle.BarMiddle
                                                             && beforeCandles.All(bf =>
                                                                 bf.Color == Quote.CandleColor.Green &&
                                                                 bf.BarTop <= firstCandle.BarBottom)
                                                             && afterCandles.All(aft =>
                                                                 aft.Color == Quote.CandleColor.Red &&
                                                                 aft.BarTop <= secondCandle.BarBottom))
            {
                patterns.Add(new CandlePattern(slice, PatternType.DarkCloudCover));
            }
        }

        private static void AddEngulfPatternIfExists(List<Candle> slice, List<CandlePattern> patterns)
        {
            var beforeCandles = slice.Take(2).ToList();
            var afterCandles = slice.Skip(4).Take(2).ToList();
            var firstCandle = slice[2];
            var secondCandle = slice[3];

            if (!secondCandle.Engulfs(firstCandle)) return; // Its not engulfing

            // Bullish
            if (firstCandle.Color == Quote.CandleColor.Red && secondCandle.Color == Quote.CandleColor.Green)
            {
                if (beforeCandles.All(bf => bf.Color == Quote.CandleColor.Red && bf.BarBottom >= firstCandle.BarTop)
                    && afterCandles.All(aft =>
                        aft.Color == Quote.CandleColor.Green && aft.BarBottom >= secondCandle.BarTop))
                {
                    patterns.Add(new CandlePattern(slice, PatternType.BullishEngulf));
                }
            }

            // Bearish
            if (firstCandle.Color == Quote.CandleColor.Green && secondCandle.Color == Quote.CandleColor.Red)
            {
                if (beforeCandles.All(bf => bf.Color == Quote.CandleColor.Green && bf.BarTop <= firstCandle.BarBottom)
                    && afterCandles.All(aft =>
                        aft.Color == Quote.CandleColor.Red && aft.BarTop <= secondCandle.BarBottom))
                {
                    patterns.Add(new CandlePattern(slice, PatternType.BearishEngulf));
                }
            }
        }

        private static void AddHammerPatternIfExists(List<Candle> slice, List<CandlePattern> patterns)
        {
            var hammerSlice = slice.Skip(slice.Count - 5).Take(5).ToList();
            var potentialHammer = hammerSlice[2];
            var beforeCandles = hammerSlice.Take(2).ToList();
            var afterCandles = hammerSlice.Skip(3).Take(2).ToList();

            if (!potentialHammer.IsHammer) return; // Its not a hammer

            // Bullish
            if (potentialHammer.Color == Quote.CandleColor.Green
                && beforeCandles.All(bf =>
                    bf.Color == Quote.CandleColor.Red && bf.BarBottom >= potentialHammer.BarBottom)
                && afterCandles.All(aft =>
                    aft.Color == Quote.CandleColor.Green && aft.BarBottom >= potentialHammer.BarTop)
               )
            {
                patterns.Add(potentialHammer.TopWickHeight < potentialHammer.BottomWickHeight
                    ? new CandlePattern(hammerSlice, PatternType.Hammer)
                    : new CandlePattern(hammerSlice, PatternType.InvertedHammer));
            }

            // Bearish
            if (potentialHammer.Color == Quote.CandleColor.Red
                && beforeCandles.All(
                    bf => bf.Color == Quote.CandleColor.Green && bf.BarBottom <= potentialHammer.BarTop)
                && afterCandles.All(aft =>
                    aft.Color == Quote.CandleColor.Red && aft.BarBottom <= potentialHammer.BarBottom)
               )
            {
                patterns.Add(potentialHammer.TopWickHeight < potentialHammer.BottomWickHeight
                    ? new CandlePattern(hammerSlice, PatternType.HangingMan)
                    : new CandlePattern(hammerSlice, PatternType.ShootingStar));
            }
        }
    }
}