﻿using System;

namespace StcokRecommender.Services.Models.Basic
{
    public class Quote // TODO rename me qoute
    {
        public enum CandleColor
        {
            Black,
            Red,
            Green
        }

        public DateTime Timestamp { get; set; }
        public decimal? Close { get; set; }
        public decimal? AdjustedClose { get; set; }

        public decimal? High { get; set; }
        public decimal? Low { get; set; }
        public decimal? Open { get; set; }
        public decimal? Volume { get; set; }
        public bool HasRegularPriceValues => High.HasValue && Low.HasValue && Open.HasValue && Close.HasValue;

        public CandleColor Color => HasRegularPriceValues
            ? Open.Value > Close.Value ? CandleColor.Red :
            Open.Value < Close.Value ? CandleColor.Green : CandleColor.Black
            : CandleColor.Black;
    }
}