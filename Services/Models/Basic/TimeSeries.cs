﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using StcokRecommender.Services.Models.YahooContracts;

namespace StcokRecommender.Services.Models.Basic
{
    public class TimeSeries
    {
        public string Interval { get; set; }
        public string Symbol { get; set; }
        public List<Quote> Series { get; set; }
        public string Currency { get; set; }

        private const int SHORT_MOVING_AVERAGE = 3;
        private const int MEDIUM_MOVING_AVERAGE = 5;
        private const int LONG_MOVING_AVERAGE = 10;

        public static TimeSeries FromRaw(string raw)
        {
            var rawObj = JsonConvert.DeserializeObject<QoutesResponse>(raw);
            var data = rawObj.chart.result[0];
            var adjustedClose = data.indicators?.adjclose?[0]?.adjclose;
            var series = ZipEntries(data.indicators.quote[0], adjustedClose ?? new List<decimal?>(),
                data.timestamp.Select(x => SecondsToDateTime(x)).ToList());
            return new TimeSeries
            {
                Symbol = data.meta.symbol,
                Series = series,
                Interval = data.meta.dataGranularity,
                Currency = data.meta.currency
            };
        }

        private static DateTime SecondsToDateTime(int seconds)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(seconds).ToLocalTime();
        }

        private static List<Quote> ZipEntries(YahooContracts.Quote pricesAndVolume, List<decimal?> adjustedClose,
            List<DateTime> timestamps)
        {
            var result = new List<Quote>();
            if (pricesAndVolume == null || timestamps == null) return result;
            if (adjustedClose == null) adjustedClose = new List<decimal?>();

            var pricesAndVolumeCount = pricesAndVolume.close.Count;
            var adjustedCloseCount = adjustedClose.Count;
            var timestampsCount = timestamps.Count;
            if (pricesAndVolumeCount != timestampsCount &&
                (adjustedCloseCount == 0 || adjustedCloseCount == timestamps.Count))
            {
                throw new Exception(
                    $"Prices/volume ({pricesAndVolumeCount}), adjustedclose ({adjustedCloseCount}) and timestamps ({timestampsCount}) not equal length");
            }

            for (int i = 0; i < pricesAndVolume.close.Count; i++)
            {
                result.Add(new Quote
                {
                    Open = pricesAndVolume.open[i],
                    Close = pricesAndVolume.close[i],
                    Low = pricesAndVolume.low[i],
                    High = pricesAndVolume.high[i],
                    Volume = pricesAndVolume.high[i],
                    AdjustedClose = adjustedCloseCount > 0 ? adjustedClose[i] : null,
                    Timestamp = timestamps[i]
                });
            }

            return result;
        }

        public bool UpwardMAs =>
            CalculateMovingAverages()
                .Select(ma => ma.Last())
                .Take(3)
                .OrderBy(val => val)
                .SequenceEqual(CalculateMovingAverages()
                    .Select(ma => ma.Last())
                    .Take(3));

        public bool IncreasingVolume
        {
            get
            {
                if (Series.Count < 3)
                {
                    return false;
                }

                var last3Days = Series.TakeLast(3).ToList();
                return last3Days.Select(x => x.Volume).OrderBy(x => x).SequenceEqual(last3Days.Select(x => x.Volume));
            }
        }

        public bool BeatingAverages
        {
            get
            {
                var currentPrice = Series.LastOrDefault().AdjustedClose;
                var movingAverages = CalculateMovingAverages();
                if (movingAverages == null || movingAverages.Count == 0)
                {
                    return false;
                }

                foreach (var avg in movingAverages)
                {
                    if (avg.Count > 0 && avg.Last() > currentPrice)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public bool IsGreenCandle
        {
            get
            {
                if (Series == null || Series.Count == 0)
                {
                    return false;
                }

                var currentDay = Series.LastOrDefault();
                if (currentDay != null)
                {
                    return currentDay.Color == Quote.CandleColor.Green;
                }

                return false;
            }
        }

        private List<List<decimal>> CalculateMovingAverages()
        {
            var days = new List<int> { SHORT_MOVING_AVERAGE, MEDIUM_MOVING_AVERAGE, LONG_MOVING_AVERAGE };
            var result = new List<List<decimal>>();
            foreach (var day in days)
            {
                result.Add(CalculateMovingAverage(day));
            }

            return result;
        }


        private List<decimal> CalculateMovingAverage(int days)
        {
            var result = new List<decimal>();
            var seriesCount = Series.Count;
            if (seriesCount == 0 || seriesCount < days)
            {
                return result;
            }

            for (int i = 0; i < seriesCount - days; i++)
            {
                var closePrices = Series.Skip(i).Take(days).Select(x => x.Close.Value).ToList();
                result.Add(closePrices.Average());
            }

            return result;
        }
    }
}