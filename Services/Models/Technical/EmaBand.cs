﻿using System;
using System.Collections.Generic;

namespace StcokRecommender.Services.Models.Technical
{
    public class EmaBand
    {
        public List<EmaEntry> Entries { get; set; }
        public int LookBack { get; set; }
        public string Symbol { get; set; }
    }

    public class EmaEntry
    {
        public DateTime Timestamp { get; set; }
        public decimal Value { get; set; }
    }
}