﻿namespace StcokRecommender.Services.Models.Technical
{
    public enum Signal
    {
        Buy,
        Sell,
        Hold
    }
}