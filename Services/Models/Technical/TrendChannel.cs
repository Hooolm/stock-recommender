﻿using System;
using System.Collections.Generic;
using System.Linq;
using StcokRecommender.Services.Models.Basic;

namespace StcokRecommender.Services.Models.Technical
{
    public class TrendChannel
    {
        public List<TrendMember> Members { get; set; } = new List<TrendMember>();
        public List<TrendMember> Highs => Members.Where(tm => tm.Type == MemberType.High).ToList();

        public List<TrendMember> Lows => Members.Where(tm => tm.Type == MemberType.Low).ToList();
        // TODO properties min, max, trendline function
    }

    public enum MemberType
    {
        High,
        Low
    }

    public class TrendMember : Quote
    {
        public TrendMember(Quote quote, MemberType type)
        {
            if (!quote.HasRegularPriceValues)
            {
                throw new Exception("Cant make a candle out of entry without regular prices");
            }

            High = quote.High;
            Low = quote.Low;
            Open = quote.Open;
            Close = quote.Close;
            AdjustedClose = quote.AdjustedClose;
            Volume = quote.Volume;
            Timestamp = quote.Timestamp;
            Type = type;
        }

        public MemberType Type { get; set; }
    }
}