﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StcokRecommender.Services.Models.Technical
{
    public enum Sentiment
    {
        Bullish,
        Bearish,
        Neutral
    }

    public enum PatternType
    {
        // Bull
        Hammer,
        InvertedHammer,
        BullishEngulf,
        PiercingLine,
        MorningStar,
        WhiteSoldiers,

        //Bear
        ShootingStar,
        HangingMan,
        BearishEngulf,
        DarkCloudCover,
        EveningStar,
        BlackCrows,

        // Neutral
        SpinningTop,
    }

    public class CandlePattern
    {
        public CandlePattern(IReadOnlyCollection<Candle> candles, PatternType type)
        {
            Type = type;
            From = candles.First().Timestamp;
            To = candles.Last().Timestamp;
        }

        public PatternType Type { get; set; }
        public Sentiment Sentiment => GetSentimentFromType(Type);

        public DateTime From { get; set; }
        public DateTime To { get; set; }

        private static Sentiment GetSentimentFromType(PatternType type)
        {
            switch (type)
            {
                case PatternType.Hammer: return Sentiment.Bullish;
                case PatternType.InvertedHammer: return Sentiment.Bullish;
                case PatternType.BullishEngulf: return Sentiment.Bullish;
                case PatternType.PiercingLine: return Sentiment.Bullish;
                case PatternType.MorningStar: return Sentiment.Bullish;
                case PatternType.WhiteSoldiers: return Sentiment.Bullish;

                case PatternType.ShootingStar: return Sentiment.Bearish;
                case PatternType.HangingMan: return Sentiment.Bearish;
                case PatternType.BearishEngulf: return Sentiment.Bearish;
                case PatternType.DarkCloudCover: return Sentiment.Bearish;
                case PatternType.EveningStar: return Sentiment.Bearish;
                case PatternType.BlackCrows: return Sentiment.Bearish;

                case PatternType.SpinningTop: return Sentiment.Neutral;
                default: throw new NotImplementedException();
            }
        }
    }
}