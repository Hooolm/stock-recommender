﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using StcokRecommender.Services.Models.Technical;

namespace StockRecommender.Models.Technical
{
    public class TouchRank
    {
        [JsonIgnore] public Guid Id { get; set; }
        public IList<Level> Levels { get; set; }
        public Signal Signal { get; set; }
    }

    public class Level
    {
        public decimal Price { get; set; }
        public decimal Score { get; set; }
    }
}