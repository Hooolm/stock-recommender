﻿using System;
using StcokRecommender.Services.Models.Basic;

namespace StcokRecommender.Services.Models.Technical
{
    public class Candle : Quote
    {
        private const decimal WickToBodyHammerRatio = 2;


        public Candle(Quote quote)
        {
            if (!quote.HasRegularPriceValues)
            {
                throw new Exception("Cant make a candle out of entry without regular prices");
            }

            High = quote.High;
            Low = quote.Low;
            Open = quote.Open;
            Close = quote.Close;
            AdjustedClose = quote.AdjustedClose;
            Volume = quote.Volume;
            Timestamp = quote.Timestamp;
        }

        public decimal TopWickTop => High.Value;
        public decimal TopWickBottom => Color == CandleColor.Green ? Close.Value : Open.Value;
        public decimal TopWickHeight => TopWickTop - TopWickBottom;

        public decimal BottomWickTop => Color == CandleColor.Green ? Open.Value : Close.Value;
        public decimal BottomWickBottom => Low.Value;
        public decimal BottomWickHeight => BottomWickTop - BottomWickBottom;


        public decimal BarTop => Color == CandleColor.Green ? Close.Value : Open.Value;
        public decimal BarBottom => Color == CandleColor.Green ? Open.Value : Close.Value;
        public decimal BarMiddle => (BarTop - BarBottom) / 2 + BarBottom;
        public decimal BarHeight => BarTop - BarBottom;

        public decimal CandleHeight => High.Value - Low.Value;

        public bool IsHammer => (Math.Max(TopWickHeight, BottomWickHeight) >= BarHeight * WickToBodyHammerRatio) &&
                                (Math.Min(TopWickHeight, BottomWickHeight) <= BarHeight * WickToBodyHammerRatio);

        public bool Engulfs(Candle candle)
        {
            return BarTop > candle.BarTop && BarBottom < candle.BarBottom;
        }
    }
}