﻿using System;

namespace StcokRecommender.Services.Models
{
    public class WebRequest
    {
        public Guid Id { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
    }
}