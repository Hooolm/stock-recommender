﻿using System;
using System.Threading.Tasks;
using StcokRecommender.Services.Models.Basic;

namespace StcokRecommender.Services
{
    public static class YahooService
    {
        public static async Task<TimeSeries> GetTimeseriesAsync(string symbol, string interval = "1d",
            DateTime? from = null, DateTime? to = null)
        {
            var mocked = TimeSeriesMock.GetMockedIfAny(symbol);
            if (mocked != null) return mocked;

            if (from == null) from = DateTime.Now.Subtract(TimeSpan.FromDays(30));
            if (to == null) to = DateTime.Now;

            var fromEpoch = GetEpoch(from.Value);
            var toEpoch = GetEpoch(to.Value);

            var url =
                $"https://query2.finance.yahoo.com/v8/finance/chart/{symbol}?symbol={symbol}&interval={interval}&period1={fromEpoch}&period2={toEpoch}";
            var raw = await CachedWebCaller.GetRequest(url);
            return TimeSeries.FromRaw(raw);
        }

        private static int GetEpoch(DateTime dateTime)
        {
            return (int)(dateTime - DateTime.UnixEpoch).TotalSeconds;
        }
    }
}