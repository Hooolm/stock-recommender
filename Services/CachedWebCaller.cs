﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StcokRecommender.Services.Models;
using WebRequest = StcokRecommender.Services.Models.WebRequest;

namespace StcokRecommender.Services
{
    public static class CachedWebCaller
    {
        public static async Task<string> GetRequest(string url, bool force = false)
        {
            using (var context = new DatabaseContext())
            {
                var request = await context.WebRequests.FirstOrDefaultAsync(r => r.Request == url);

                if (force && request != null)
                {
                    context.WebRequests.Remove(request);
                    await context.SaveChangesAsync();
                }

                if (request == null || force)
                {
                    var response = await MakeCallUrl(url);

                    request = new WebRequest()
                    {
                        Request = url,
                        Response = response
                    };

                    await context.WebRequests.AddAsync(request);
                    await context.SaveChangesAsync();
                }

                return request.Response;
            }
        }

        private static async Task<string> MakeCallUrl(string url)
        {
            var request = System.Net.WebRequest.CreateHttp(url);
            var response = (HttpWebResponse)await request.GetResponseAsync();

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpRequestException();
            }

            using (Stream stream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                var rawJson = reader.ReadToEnd();
                return rawJson;
            }
        }
    }
}