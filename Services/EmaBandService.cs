﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StcokRecommender.Services.Models.Basic;
using StcokRecommender.Services.Models.Technical;

namespace StcokRecommender.Services
{
    public static class EmaBandService
    {
        public static async Task<EmaBand> GetEmaBand(string symbol, string interval, int lookBack, int? take = null,
            DateTime? from = null, DateTime? to = null)
        {
            if (take == null)
            {
                take = lookBack;
            }

            if (from == null)
            {
                from = DateTime.Now;
            }

            if (interval == "1d" && from != null)
            {
                from = from.Value.AddDays(-lookBack * 2);
            }

            if (interval == "1h" && from != null)
            {
                from = from.Value.AddHours(-lookBack * 2);
            }

            return GetTrendChannel(await YahooService.GetTimeseriesAsync(symbol, interval, from, to), lookBack,
                take.Value);
        }

        public static EmaBand GetTrendChannel(TimeSeries series, int lookBack, int take)
        {
            var eb = new EmaBand
            {
                Symbol = series.Symbol,
                LookBack = lookBack,
            };

            var alpha = 2m / (lookBack + 1m);
            var values = series.Series.Select(x => x.Close ?? 0m).ToList();

            var emaValues = new List<decimal>();

            for (int i = 0; i < values.Count; i++)
            {
                if (i == 0)
                {
                    emaValues.Add(values[i]);
                }
                else
                {
                    emaValues.Add((values[i] * alpha) + (1 - alpha) * emaValues[i - 1]);
                }
            }

            eb.Entries = emaValues.Zip(series.Series, (a, b) => new EmaEntry { Value = a, Timestamp = b.Timestamp })
                .ToList();

            if (take != lookBack)
            {
                eb.Entries = eb.Entries.TakeLast(take).ToList();
            }

            return eb;
        }

        public static decimal CaclEma(IEnumerable<decimal> qoutes, decimal alpha)
        {
            return qoutes.Aggregate(qoutes.First(), (ema, current) => alpha * current + (1m - alpha) * ema);
        }
    }
}