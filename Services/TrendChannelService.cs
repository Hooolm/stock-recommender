﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StcokRecommender.Services.Models.Basic;
using StcokRecommender.Services.Models.Technical;

namespace StcokRecommender.Services
{
    public class TrendChannelService
    {
        private const double MinDistanceRatio = 2d / 100d;

        public static async Task<TrendChannel> GetTrendChannel(string symbol, string interval, DateTime? from = null,
            DateTime? to = null)
        {
            return GetTrendChannel(await YahooService.GetTimeseriesAsync(symbol, interval, from, to));
        }

        public static TrendChannel GetTrendChannel(TimeSeries series)
        {
            var tc = new TrendChannel();
            // TODO Consider if 5 is just superior
            var minDistance = 5; //(int)Math.Round(series.Series.Count() * MinDistanceRatio);
            var lows = FindHighLows(series.Series, minDistance, false);
            var highs = FindHighLows(series.Series, minDistance, true);
            tc.Members.AddRange(lows.Concat(highs));

            return tc;
        }

        private static List<TrendMember> FindHighLows(List<Quote> entries, int minDistance, bool findHigh,
            List<Quote> accumulated = null, Dictionary<DateTime, bool> banned = null)
        {
            if (accumulated == null)
            {
                accumulated = new List<Quote>();
            }

            if (banned == null)
            {
                banned = new Dictionary<DateTime, bool>();
                foreach (var entry in entries)
                {
                    banned.Add(entry.Timestamp, false);
                }
            }

            if (entries.Count == 0)
                return accumulated.Select(ent => new TrendMember(ent, findHigh ? MemberType.High : MemberType.Low))
                    .ToList();

            Quote best;
            if (findHigh)
            {
                best = entries.OrderByDescending(ent => (ent.High ?? decimal.MinValue)).FirstOrDefault();
            }
            else
            {
                best = entries.OrderBy(ent => (ent.Low ?? decimal.MaxValue)).FirstOrDefault();
            }

            var indexOfBest = entries.IndexOf(best);
            var amountBefore = Math.Max(indexOfBest - minDistance, 0);

            var bannedSlice = entries.Skip(amountBefore).Take(minDistance * 2 + 1);

            var allowed = true;
            foreach (var entry in bannedSlice)
            {
                if (banned[entry.Timestamp])
                {
                    allowed = false;
                }
            }

            if (!allowed)
            {
                banned[best.Timestamp] = true;
                entries = entries.Take(amountBefore).Concat(entries.Skip(amountBefore + 1)).ToList();
            }
            else
            {
                foreach (var entry in bannedSlice)
                {
                    banned[entry.Timestamp] = true;
                }

                var before = entries.Take(amountBefore);
                var after = entries.Skip(indexOfBest + 1 + minDistance);

                entries = before.Concat(after).ToList();

                accumulated.Add(best);
            }


            return FindHighLows(entries, minDistance, findHigh, accumulated);
        }
    }
}