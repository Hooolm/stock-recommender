﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StcokRecommender.Services.Models;
using StcokRecommender.Services.Models.Basic;
using StcokRecommender.Services.Models.Technical;
using StockRecommender.Models.Technical;

namespace StcokRecommender.Services
{
    public static class TouchRankService
    {
        internal class TouchRankConfig
        {
            public TouchRankConfig(List<decimal> prices, List<decimal> volumes)
            {
                Prices = prices;
                Volumes = volumes;

                Chunks = 1000;
                Candidates = 3;
                ScoreThreshold = 0;

                Length = prices.Count;
                Min = prices.Min();
                Max = prices.Max();
                Diff = Max - Min;
            }

            public List<decimal> Prices { get; set; }
            public List<decimal> Volumes { get; set; }
            public decimal Min { get; set; }
            public decimal Max { get; set; }
            public decimal Diff { get; set; }
            public int Length { get; set; }
            public int Chunks { get; set; }
            public int Depth { get; set; }
            public int Candidates { get; set; }
            public decimal ScoreThreshold { get; set; }
        }

        public static async Task<TouchRank> GetTouchRank(string symbol, string interval, DateTime? from = null,
            DateTime? to = null)
        {
            return await GetTouchRank(await YahooService.GetTimeseriesAsync(symbol, interval, from, to));
        }

        public static async Task<TouchRank> GetTouchRank(TimeSeries series)
        {
            var prices = series.Series.Where(x => x.HasRegularPriceValues).Select(x => x.Close.Value).ToList();
            var volumes = series.Series.Where(x => x.HasRegularPriceValues).Select(x => x.Volume.Value).ToList();
            var config = new TouchRankConfig(prices, volumes);

            var levels = await GetLevels(config);

            var tr = new TouchRank
            {
                Levels = levels.Select(h => new Level { Price = h.Price, Score = h.Score }).ToList()
            };

            using (var context = new DatabaseContext())
            {
                context.TouchRanks.Add(tr);
                await context.SaveChangesAsync();
            }


            var currentPrice = series.Series.Last().Close;
            var lookBackRate = 2;
            var priceLookBack = series.Series[series.Series.Count - lookBackRate].Close;


            var aboves = tr.Levels.Where(l => l.Price > currentPrice).ToList();
            var belows = tr.Levels.Where(l => l.Price < currentPrice).ToList();

            tr.Signal = Signal.Hold;

            if (!belows.Any())
            {
                tr.Signal = Signal.Sell;
            }
            else if (aboves.Any() && belows.Any())
            {
                var closestAbove = tr.Levels.Where(l => l.Price > currentPrice).Min(x => x.Price);
                var closestBelow = tr.Levels.Where(l => l.Price < currentPrice).Max(x => x.Price);

                if (priceLookBack > closestAbove)
                {
                    tr.Signal = Signal.Sell;
                }
                else if (priceLookBack < closestBelow)
                {
                    tr.Signal = Signal.Buy;
                }
            }

            return tr;
        }

        private static async Task<List<Level>> GetLevels(TouchRankConfig config)
        {
            var chunkSize = config.Diff / config.Chunks;
            var canditaeSize = config.Diff / config.Candidates;

            var levelsTasks = new List<Task<Level>>();
            for (int i = 0; i < config.Chunks; i++)
            {
                var testingPrice = config.Min + i * chunkSize;
                levelsTasks.Add(Task.Run(() => ScoreHorizontal(config, testingPrice)));
            }

            var levels = (await Task.WhenAll(levelsTasks)).ToList();

            levels = NormalizeFilterAndSort(levels, config);

            var results = new List<Level>();

            foreach (var level in levels)
            {
                if (results.Count == config.Candidates) break;

                if (results.Where(x => Math.Abs(x.Price - level.Price) <= canditaeSize).Any()) continue;

                results.Add(level);
            }

            return results;
        }

        private static List<Level> NormalizeFilterAndSort(List<Level> levels, TouchRankConfig config)
        {
            var highScore = levels.Max(x => x.Score);
            // Normalize score
            for (int i = 0; i < levels.Count; i++)
            {
                levels[i].Score = levels[i].Score / highScore;
            }

            // Filter out too low scores and sort
            return levels.Where(l => l.Score >= config.ScoreThreshold).OrderByDescending(x => x.Score).ToList();
        }

        private static Level ScoreHorizontal(TouchRankConfig config, decimal testingPrice)
        {
            var length = config.Length;
            var horizontal = new Level { Price = testingPrice };
            var beforeAfterSize = (int)Math.Max(Math.Ceiling(config.Length / 10d), 1);

            var entryScores = new List<decimal>();
            for (int i = 0; i < length; i++)
            {
                if (length - beforeAfterSize - 1 == i)
                {
                    continue;
                }

                var currentPrice = config.Prices[i];

                var after = config.Prices.Skip(i + 1).Take(beforeAfterSize).ToList()
                    .Zip(config.Volumes.Skip(i + 1).Take(beforeAfterSize).ToList(), (a, b) => (a, b)).ToList();
                entryScores.Add(ScoreEntry(testingPrice, config.Diff, currentPrice, after));
            }

            horizontal.Score = entryScores.Sum();
            return horizontal;
        }

        private static decimal ScoreEntry(decimal testingPrice, decimal diff, decimal price,
            List<(decimal price, decimal volume)> after)
        {
            var score = 0m;

            // To avoid divide by zero
            var closeness = Math.Max(Math.Abs(testingPrice - price), 0.0001m);
            score += diff / closeness * after.Sum(aft => Math.Abs(aft.price - price) * aft.volume);

            return score;
        }
    }
}