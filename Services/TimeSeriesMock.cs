﻿using System;
using System.Collections.Generic;
using StcokRecommender.Services.Models.Basic;

namespace StcokRecommender.Services
{
    public static class TimeSeriesMock
    {
        public static TimeSeries GetMockedIfAny(string symbol)
        {
            switch (symbol)
            {
                case "MOCK.HAMMER": return Hammer();
                case "MOCK.INVERTED_HAMMER": return InvertedHammer();
                case "MOCK.SHOOTING_STAR": return ShootingStar();
                case "MOCK.HANGING_MAN": return HangingMan();
                case "MOCK.DARK_CLOUD_COVER": return DarkCloudCover();
                default: return null;
            }
        }

        public static TimeSeries Hammer()
        {
            var ts = new TimeSeries
            {
                Currency = "USD",
                Interval = "1d",
                Symbol = "MOCK.GONK",
                Series = new List<Quote>(),
            };

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 01),
                High = 100,
                Open = 100,
                Low = 100,
                Close = 100,
                Volume = 100
            });


            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 02),
                High = 100,
                Open = 100,
                Low = 90,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 03),
                High = 90,
                Open = 90,
                Low = 80,
                Close = 80,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 04),
                High = 80,
                Open = 70,
                Low = 50,
                Close = 80,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 05),
                High = 90,
                Open = 80,
                Low = 80,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 06),
                High = 100,
                Open = 90,
                Low = 90,
                Close = 100,
                Volume = 100
            });

            return ts;
        }

        public static TimeSeries InvertedHammer()
        {
            var ts = new TimeSeries
            {
                Currency = "USD",
                Interval = "1d",
                Symbol = "MOCK.GONK",
                Series = new List<Quote>(),
            };

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 01),
                High = 100,
                Open = 100,
                Low = 100,
                Close = 100,
                Volume = 100
            });


            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 02),
                High = 100,
                Open = 100,
                Low = 90,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 03),
                High = 90,
                Open = 90,
                Low = 80,
                Close = 80,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 04),
                High = 100,
                Open = 70,
                Low = 70,
                Close = 80,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 05),
                High = 90,
                Open = 80,
                Low = 80,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 06),
                High = 100,
                Open = 90,
                Low = 90,
                Close = 100,
                Volume = 100
            });

            return ts;
        }

        public static TimeSeries HangingMan()
        {
            var ts = new TimeSeries
            {
                Currency = "USD",
                Interval = "1d",
                Symbol = "MOCK.GONK",
                Series = new List<Quote>(),
            };

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 01),
                High = 100,
                Open = 100,
                Low = 100,
                Close = 100,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 02),
                High = 90,
                Open = 80,
                Low = 80,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 03),
                High = 100,
                Open = 90,
                Low = 90,
                Close = 100,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 04),
                High = 100,
                Open = 100,
                Low = 70,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 05),
                High = 100,
                Open = 100,
                Low = 90,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 06),
                High = 90,
                Open = 90,
                Low = 80,
                Close = 80,
                Volume = 100
            });

            return ts;
        }

        public static TimeSeries ShootingStar()
        {
            var ts = new TimeSeries
            {
                Currency = "USD",
                Interval = "1d",
                Symbol = "MOCK.GONK",
                Series = new List<Quote>(),
            };

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 01),
                High = 100,
                Open = 100,
                Low = 100,
                Close = 100,
                Volume = 100
            });


            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 02),
                High = 90,
                Open = 80,
                Low = 80,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 03),
                High = 100,
                Open = 90,
                Low = 90,
                Close = 100,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 04),
                High = 120,
                Open = 100,
                Low = 85,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 05),
                High = 100,
                Open = 100,
                Low = 90,
                Close = 90,
                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 06),
                High = 90,
                Open = 90,
                Low = 80,
                Close = 80,
                Volume = 100
            });

            return ts;
        }

        public static TimeSeries DarkCloudCover()
        {
            var ts = new TimeSeries
            {
                Currency = "USD",
                Interval = "1d",
                Symbol = "MOCK.GONK",
                Series = new List<Quote>(),
            };

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 01),
                Open = 50,
                Close = 60,

                Low = 50,
                High = 60,

                Volume = 100
            });


            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 02),
                Open = 60,
                Close = 70,

                Low = 60,
                High = 70,

                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 03),
                Open = 70,
                Close = 90,

                Low = 70,
                High = 100,

                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 04),
                Open = 90,
                Close = 75,

                Low = 70,
                High = 100,

                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 05),
                Open = 75,
                Close = 60,

                Low = 60,
                High = 75,

                Volume = 100
            });

            ts.Series.Add(new Quote
            {
                Timestamp = new DateTime(2021, 01, 06),
                Open = 60,
                Close = 50,

                Low = 50,
                High = 60,

                Volume = 100
            });


            return ts;
        }
    }
}