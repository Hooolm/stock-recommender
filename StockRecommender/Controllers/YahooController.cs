﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StcokRecommender.Services;

namespace StockRecommender.Controllers
{
    [Route("api/yahoo")]
    [ApiController]
    public class YahooController : ControllerBase
    {
        [HttpGet("quotes")]
        public async Task<IActionResult> Quotes(string symbol, string interval = "1d", DateTime? from = null,
            DateTime? to = null)
        {
            if (string.IsNullOrEmpty(symbol))
            {
                return new BadRequestResult();
            }

            return Ok(await YahooService.GetTimeseriesAsync(symbol, interval, from, to));
        }

        [HttpGet("search")]
        public async Task<IActionResult> Ticker(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                return new BadRequestResult();
            }

            var url =
                $"https://query2.finance.yahoo.com/v1/finance/search?quotesCount=5&newsCount=0&enableFuzzyQuery=true&q={query}";
            var result = await CachedWebCaller.GetRequest(url);
            return Ok(result);
        }

        [HttpGet("summary")]
        public async Task<IActionResult> Summary(string symbol)
        {
            if (string.IsNullOrEmpty(symbol))
            {
                return new BadRequestResult();
            }

            var url = $"https://query2.finance.yahoo.com/v10/finance/quoteSummary/{symbol}?modules=price";
            var result = await CachedWebCaller.GetRequest(url, true);
            return Ok(result);
        }
    }
}