﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StcokRecommender.Services;

namespace StockRecommender.Controllers
{
    [Route("api/technical")]
    [ApiController]
    public class TechnicalController : ControllerBase
    {
        [HttpGet("touch-ranks")]
        public async Task<IActionResult> TouchRank(string symbol, string interval = "1d", DateTime? from = null,
            DateTime? to = null)
        {
            if (symbol == null)
            {
                return new BadRequestResult();
            }

            return Ok(await TouchRankService.GetTouchRank(symbol, interval, from, to));
        }

        [HttpGet("candle-patterns")]
        public async Task<IActionResult> CandlePatterns(string symbol, string interval = "1d", DateTime? from = null,
            DateTime? to = null)
        {
            if (symbol == null)
            {
                return new BadRequestResult();
            }

            return Ok(await CandlePatternService.GetCandlePatternsAsync(symbol, interval, from, to));
        }


        [HttpGet("trend-channel")]
        public async Task<IActionResult> TrendChannel(string symbol, string interval = "1d", DateTime? from = null,
            DateTime? to = null)
        {
            if (symbol == null)
            {
                return new BadRequestResult();
            }

            return Ok(await TrendChannelService.GetTrendChannel(symbol, interval, from, to));
        }

        [HttpGet("ema-band")]
        public async Task<IActionResult> EmaBand(string symbol, string interval = "1d", int? take = null,
            DateTime? from = null, DateTime? to = null, int lookBack = 9)
        {
            if (symbol == null)
            {
                return new BadRequestResult();
            }

            return Ok(await EmaBandService.GetEmaBand(symbol, interval, lookBack, take, from, to));
        }
        
        [HttpGet("buy-rating")]
        public async Task<IActionResult> BuyRating(string symbol)
        {
            if (symbol == null)
            {
                return new BadRequestResult();
            }

            var interval = "1wk";
            var to = DateTime.Now;
            var from = to.Subtract(TimeSpan.FromDays(1000));
            var ts = await YahooService.GetTimeseriesAsync(symbol, interval, from, to);
            
            dynamic response = new
            {
                BeatingAverages = ts.BeatingAverages,
                IncreasingVolume = ts.IncreasingVolume,
                IsGreenCandle = ts.IsGreenCandle,
                UpwardMAs = ts.UpwardMAs,
                Interval = interval,
                From = from,
                To = to,
                Symbol = symbol,
            };

            return Ok(response);
        }
    }
}