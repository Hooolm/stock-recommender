import React, { useEffect, useRef, useState } from 'react'
import { CandlePatternType, EmaBand, TimeSeries, TouchRankType, TrendChannel } from 'src/api'
import { useWindowDimensions } from 'src/hooks/useWindowsDimensions'
import { theme } from 'src/theme'
import styled from 'styled-components'
import { P2 } from '../typography/Paragraph'
import { Axises, XLabelRange, XLabelType } from './Axises'

const GraphLayout = {
  graphPadding: 0.002,
  yAxisPadding: '3rem',
  xAxisPadding: '3rem',
  xLabelWidth: '80px',
  xLabelCount: Math.floor(window.innerWidth / 80 / 2),
  minYLabelCount: 5,
}

const ChartContainer = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
`

const CandlePatternIndicator = styled.div<{
  sentiment: 'bearish' | 'bullish' | 'neutral'
  fromPct: number
  toPct: number
}>`
  background: ${(p) =>
    p.sentiment === 'bearish'
      ? p.theme.colors.red[600]
      : p.sentiment === 'bullish'
      ? p.theme.colors.green[600]
      : p.theme.colors.deepOrange[600]}15;
  position: absolute;
  left: ${(p) => p.fromPct * 100}%;
  width: ${(p) => (p.toPct - p.fromPct) * 100}%;
  height: 100%;
  display: flex;
  align-items: flex-end;
  justify-content: center;
  overflow: visible;
`

const Chart = styled.div`
  width: calc(100% - ${GraphLayout.yAxisPadding});
  height: calc(100% - ${GraphLayout.xAxisPadding});
  box-sizing: border-box;
  display: flex;
  position: absolute;
  left: 0;
  margin: 0 ${GraphLayout.yAxisPadding} ${GraphLayout.xAxisPadding} 0;
  overflow: none;
`

const Slice = styled.div<{ splitter?: boolean }>`
  flex-grow: 1;
  box-sizing: border-box;
  border-right: ${(p) => (p.splitter ? '1px solid rgba(255,255,255,0.05)' : 'none')};

  &:last-child {
    border-right: none;
  }
  display: flex;
  position: relative;
`
const Candle = styled.div`
  flex-grow: 1;
  margin: 0 20%;
  box-sizing: border-box;
  position: relative;
`
const CandleBar = styled.div<{ highest: number; lowest: number; sentiment: 'bull' | 'bear' | 'neutral' }>`
  position: absolute;
  width: 100%;
  height: ${(p) => p.highest * 100 - p.lowest * 100}%;
  min-height: 1px;
  background: ${(p) =>
    p.sentiment === 'bull'
      ? p.theme.colors.green[600]
      : p.sentiment === 'bear'
      ? p.theme.colors.red[600]
      : p.theme.colors.grey[600]};
  bottom: ${(p) => p.lowest * 100}%;
`
const CandleWick = styled.div<{ highest: number; lowest: number; sentiment: 'bull' | 'bear' | 'neutral' }>`
  position: absolute;
  width: 1px;
  left: calc(50% - 1px);
  height: ${(p) => p.highest * 100 - p.lowest * 100}%;
  background: ${(p) =>
    p.sentiment === 'bull'
      ? p.theme.colors.green[600]
      : p.sentiment === 'bear'
      ? p.theme.colors.red[600]
      : p.theme.colors.grey[600]};
  bottom: ${(p) => p.lowest * 100}%;
`

const HorizontalIndicator = styled.div<{
  placemenmt: number
  type: 'touch-rank' | 'price' | 'touch-rank-shadow'
  width?: number
}>`
  width: ${(p) => (p.width ? 100 * p.width : '100')}%;
  height: ${(p) => (p.type === 'price' ? `1px` : `2px`)};
  background: ${(p) => (p.type === 'price' ? `rgba(255, 255, 255, 0.02)` : p.theme.colors.teal[700])};
  bottom: ${(p) => 100 * p.placemenmt}%;
  position: absolute;
  opacity: ${(p) => (p.type === 'touch-rank-shadow' ? 0.15 : 1)};
`

const Indicator = styled.div`
  position: absolute;
  width: 13px;
  height: 13px;
  border-radius: 20px;
  left: -6px;
  z-index: 10;
  box-sizing: border-box;
  opacity: 0.5;
`

const HighIndicator = styled(Indicator)`
  background: ${(p) => p.theme.colors.teal[700]};
  top: -6px;
`
const LowIndicator = styled(Indicator)`
  background: ${(p) => p.theme.colors.purple[700]};
  bottom: -6px;
`

const EmaSvg = styled.svg`
  position: absolute;
  left: 0;
  top: 0;
`

export const Graph: React.FC<{
  timeSeries: TimeSeries
  trendChannel?: TrendChannel
  touchRank?: TouchRankType
  candlePatterns: CandlePatternType[]
  emaBands: EmaBand[]
}> = ({ timeSeries, trendChannel, touchRank, candlePatterns, emaBands }) => {
  const allPrices = [
    ...timeSeries.series.map((x) => x.open),
    ...timeSeries.series.map((x) => x.close),
    ...timeSeries.series.map((x) => x.low),
    ...timeSeries.series.map((x) => x.high),
  ].filter((x) => x) as number[]
  const min = Math.min(...allPrices) * (1 - GraphLayout.graphPadding)
  const max = Math.max(...allPrices) * (1 + GraphLayout.graphPadding)
  const getRelativePrice = (price: number) => {
    return (price - min) / (max - min)
  }

  const [chartSize, setChartSize] = useState({ height: 100, width: 100 })
  const chartRef = useRef<HTMLDivElement>(null)
  const windowSize = useWindowDimensions()
  useEffect(() => {
    if (chartRef === null) return
    const height = chartRef?.current?.clientHeight
    const width = chartRef.current?.clientWidth
    if (height && width) {
      setChartSize({ width, height })
    }
  }, [chartRef, windowSize])

  const getYLabelsOld = (chunks: number) => {
    let res = [] as number[]
    for (let i = 1; i < chunks; i++) {
      res = [...res, ((max - min) / chunks) * i + min]
    }
    return res.map((v) => ({ price: v, placement: getRelativePrice(v) }))
  }

  const getYLabels = (chunks: number) => {
    const largestChunk = 1000000
    const smallestChunk = 0.025
    let chunkSize = largestChunk
    const diff = max - min
    let satisfied = false
    let step = 0
    while (!satisfied) {
      const possibleChunks = diff / chunkSize

      if (possibleChunks >= chunks) {
        satisfied = true
      } else if (step < 2) {
        chunkSize = chunkSize / 2
        step++
      } else {
        chunkSize = (chunkSize * 4) / 10
        step = 0
      }
      if (chunkSize === smallestChunk) {
        break
      }
    }
    const localMin = Math.ceil(min / chunkSize) * chunkSize
    const localMax = Math.floor(max / chunkSize) * chunkSize

    let res = [] as number[]
    for (let i = localMin; i <= localMax; i += chunkSize) {
      res = [...res, i]
    }

    if (res.length === 0) {
      return getYLabelsOld(chunks)
    }

    return res.map((v) => ({ price: v, placement: getRelativePrice(v) }))
  }

  const getXLabels = (): XLabelType[] => {
    const timeDiff =
      (timeSeries.series[timeSeries.series.length - 1].timestamp.getTime() - timeSeries.series[0].timestamp.getTime()) /
      1000
    const days = timeDiff / (3600 * 24)
    const months = timeDiff / (3600 * 24 * 30.44)
    let displayChanges = [] as XLabelRange[]

    if (months < 13) {
      displayChanges = [...displayChanges, 'month' as const]
    }
    if (months <= 2) {
      displayChanges = [...displayChanges, 'day' as const]
    }
    if (days <= 2) {
      displayChanges = [...displayChanges, 'hour' as const]
    }

    return timeSeries.series.map((x, i) => {
      let label = { type: undefined, enabled: false, date: x.timestamp } as XLabelType
      if (i < timeSeries.series.length - 1) {
        const current = timeSeries.series[i].timestamp
        const next = timeSeries.series[i + 1].timestamp

        label.date = next
        if (next.getFullYear() > current.getFullYear()) {
          label.type = 'year'
          label.enabled = displayChanges.indexOf('year') !== -1
        }

        if ((next.getMonth() === 0 && current.getMonth() !== 0) || next.getMonth() > current.getMonth()) {
          label.type = label.type ?? 'month'
          label.enabled = label.enabled || displayChanges.indexOf('month') !== -1
        }

        if ((next.getDate() === 1 && current.getDate() !== 1) || next.getDate() > current.getDate()) {
          label.type = label.type ?? 'day'
          label.enabled = label.enabled || displayChanges.indexOf('day') !== -1
        }

        if ((next.getHours() === 0 && current.getHours() !== 0) || next.getHours() > current.getHours()) {
          label.type = label.type ?? 'hour'
          label.enabled = label.enabled || displayChanges.indexOf('hour') !== -1
        }
      }
      return label
    })
  }

  const yLabels = getYLabels(GraphLayout.minYLabelCount)
  const xLabels = getXLabels()
  const levels = touchRank?.levels ?? []
  const IsHigh = (seriesIndex: number) => {
    if (!timeSeries && !trendChannel) return false
    const stamp = timeSeries?.series[seriesIndex].timestamp
    return (trendChannel?.highs.filter((h) => h.timestamp.toISOString() === stamp.toISOString()).length ?? 0) > 0
  }

  const IsLow = (seriesIndex: number) => {
    if (!timeSeries && !trendChannel) return false
    const stamp = timeSeries?.series[seriesIndex].timestamp
    return (trendChannel?.lows.filter((h) => h.timestamp.toISOString() === stamp.toISOString()).length ?? 0) > 0
  }

  const lookBackToClolor = (lookback: number) => {
    switch (lookback) {
      case 2:
        return theme.colors.red[900]
      case 3:
        return theme.colors.amber[900]
      case 5:
        return theme.colors.amber[400]
      case 8:
        return theme.colors.purple[400]
      case 13:
        return theme.colors.teal[800]
      case 21:
        return theme.colors.cyan[600]
      default:
        return theme.colors.grey[200]
    }
  }
  const bands = emaBands.map((band) => {
    const lengthDiff = timeSeries.series.length - band.entries.length
    const sliceWidht = chartSize.width / timeSeries.series.length
    const path = band.entries
      .map((ema, i) => {
        const y = (1 - getRelativePrice(ema.value)) * chartSize.height
        const x = sliceWidht * (i + lengthDiff - 1) + 1.5 * sliceWidht
        const pathType = i === 0 ? 'M' : 'L'

        return pathType + ' ' + x.toString() + ',' + y.toString()
      })
      .join(' ')
    const color = lookBackToClolor(band.lookBack)
    return { ...band, path, color }
  })

  return (
    <>
      <ChartContainer>
        <Chart ref={chartRef}>
          {yLabels.map((l) => (
            <HorizontalIndicator key={l.price} placemenmt={l.placement} type={'price'} />
          ))}

          {timeSeries.series.map((entry, i) => {
            const canDrawCandle = entry.high && entry.low && entry.open && entry.close
            const sentiment =
              (entry.close as number) > (entry.open as number)
                ? 'bull'
                : (entry.close as number) < (entry.open as number)
                ? 'bear'
                : 'neutral'
            return (
              <Slice key={'slice' + entry.timestamp.toISOString() + timeSeries.symbol} splitter={xLabels[i]?.enabled}>
                {canDrawCandle && (
                  <Candle>
                    <CandleWick
                      highest={getRelativePrice(entry.high as number)}
                      lowest={getRelativePrice(entry.low as number)}
                      sentiment={sentiment}
                    >
                      {IsHigh(i) && <HighIndicator />}
                      {IsLow(i) && <LowIndicator />}
                    </CandleWick>
                    <CandleBar
                      highest={Math.max(
                        getRelativePrice(entry.open as number),
                        getRelativePrice(entry.close as number)
                      )}
                      lowest={Math.min(getRelativePrice(entry.open as number), getRelativePrice(entry.close as number))}
                      sentiment={sentiment}
                    ></CandleBar>
                  </Candle>
                )}
              </Slice>
            )
          })}
          <EmaSvg width={chartSize.width} height={chartSize.height}>
            {bands.map((b, i) => {
              return (
                <path
                  d={b.path}
                  stroke={b.color}
                  viewBox="0 0 100 100"
                  preserveAspectRatio="none"
                  fillOpacity="0"
                  strokeOpacity="0.7"
                  stroke-width="2"
                  stroke-linejoin="round"
                />
              )
            })}
          </EmaSvg>

          {levels.map((level, i) => (
            <>
              {level.price > min && level.price < max && (
                <>
                  <HorizontalIndicator
                    key={'touch-rank' + level.price}
                    placemenmt={getRelativePrice(level.price)}
                    width={level.score / levels.sort((x) => x.score)[0].score}
                    type={'touch-rank'}
                  />
                  <HorizontalIndicator
                    key={'touch-rank-shadow' + level.price}
                    placemenmt={getRelativePrice(level.price)}
                    type={'touch-rank-shadow'}
                  />
                </>
              )}
            </>
          ))}
          {candlePatterns
            .map((cp) => {
              const afterFrom = timeSeries.series.filter((x) => x.timestamp >= cp.from)
              const beforeTo = timeSeries.series.filter((x) => x.timestamp <= cp.to)
              return {
                ...cp,
                fromPct: (timeSeries.series.indexOf(afterFrom[0]) + 1) / timeSeries.series.length,
                toPct: (timeSeries.series.indexOf(beforeTo[beforeTo.length - 1]) + 1) / timeSeries.series.length,
              }
            })
            .map((cpFormatted) => {
              return (
                <CandlePatternIndicator
                  sentiment={cpFormatted.sentiment}
                  fromPct={cpFormatted.fromPct}
                  toPct={cpFormatted.toPct}
                >
                  <P2>{cpFormatted.type}</P2>
                </CandlePatternIndicator>
              )
            })}
        </Chart>
        <Axises.X
          labels={xLabels}
          xAxisPadding={GraphLayout.xAxisPadding}
          yAxisPadding={GraphLayout.yAxisPadding}
          xLabelWidth={GraphLayout.xLabelWidth}
        />
        <Axises.Y
          labels={yLabels}
          xAxisPadding={GraphLayout.xAxisPadding}
          yAxisPadding={GraphLayout.yAxisPadding}
          levels={levels
            .map((x) => ({ price: x.price, placement: getRelativePrice(x.price) }))
            .filter((x) => x.price > min && x.price < max)}
          current={{
            price: timeSeries.series[timeSeries.series.length - 1].close ?? 0,
            placement: getRelativePrice(timeSeries.series[timeSeries.series.length - 1].close ?? 0),
          }}
        />
      </ChartContainer>
    </>
  )
}
