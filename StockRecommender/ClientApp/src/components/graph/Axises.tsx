import React from 'react'
import { useWindowDimensions } from 'src/hooks/useWindowsDimensions'
import styled from 'styled-components'
import { P1, P2 } from '../typography/Paragraph'

const XAxis = styled.div<{ yAxisPadding: string; xAxisPadding: string }>`
  width: calc(100% - ${(p) => p.yAxisPadding});
  height: ${(p) => p.xAxisPadding};
  box-sizing: border-box;
  position: absolute;
  bottom: 0;
  display: flex;
  flex-direction: row;
  border-top: 1px solid #444;
`

const XLabel = styled.div<{ hidden: boolean }>`
  flex-grow: 1;
  text-align: center;
  width: 100%;
  display: flex;
  opacity: ${(p) => (p.hidden ? '0' : '1')};
  align-items: center;
  justify-content: center;
  position: relative;
`

const XLabelContents = styled.div<{ xLabelWidth: string }>`
  position: absolute;
  width: ${(p) => p.xLabelWidth};
  right: calc(${(p) => p.xLabelWidth} / 2 * -1);
`

export type XLabelRange = 'year' | 'month' | 'day' | 'hour' | undefined
export type XLabelType = { type: XLabelRange; enabled: boolean; date: Date }

const getMonthName = (month: number) => {
  switch (month) {
    case 0:
      return 'Janurary'
    case 1:
      return 'February'
    case 2:
      return 'March'
    case 3:
      return 'April'
    case 4:
      return 'May'
    case 5:
      return 'June'
    case 6:
      return 'July'
    case 7:
      return 'August'
    case 8:
      return 'September'
    case 9:
      return 'October'
    case 10:
      return 'November'
    default:
      return 'December'
  }
}

const X: React.FC<{
  yAxisPadding: string
  xAxisPadding: string
  xLabelWidth: string
  labels: XLabelType[]
}> = ({ yAxisPadding, xAxisPadding, xLabelWidth, labels }) => {
  const { width } = useWindowDimensions()

  const enabledLabels = labels.filter((l) => l.enabled).length

  var skipEvery = 1
  if (width < enabledLabels * 100) {
    skipEvery = 2
  }

  if (width < enabledLabels * 50) {
    skipEvery = 4
  }

  if (width < enabledLabels * 25) {
    skipEvery = 8
  }

  var j = 0

  return (
    <XAxis yAxisPadding={yAxisPadding} xAxisPadding={xAxisPadding}>
      {labels.map((label, i) => {
        if (label.enabled) {
          j++
        }

        return (
          <XLabel key={'xlabel' + i} hidden={j % skipEvery !== 0}>
            {label.enabled && (
              <XLabelContents xLabelWidth={xLabelWidth}>
                <P2>
                  {label.type === 'year' && label.date.getFullYear()}
                  {label.type === 'month' && getMonthName(label.date.getMonth())}
                  {label.type === 'day' && label.date.getDate()}
                  {label.type === 'hour' && label.date.getHours() + ':00'}
                </P2>
              </XLabelContents>
            )}
          </XLabel>
        )
      })}
    </XAxis>
  )
}

const YAxis = styled.div<{ yAxisPadding: string; xAxisPadding: string }>`
  width: ${(p) => p.yAxisPadding};
  height: calc(100% - ${(p) => p.xAxisPadding});
  box-sizing: border-box;
  position: absolute;
  bottom: ${(p) => p.xAxisPadding};
  right: 0;
  border-left: 1px solid #444;
`
const YLabel = styled(P1)<{ placement: number; type?: 'current' | 'level' }>`
  bottom: calc(${(p) => p.placement * 100}% - 0.1rem);
  padding: 0.1rem;
  width: 100%;
  position: absolute;
  background: ${(p) =>
    p.type ? (p.type === 'current' ? p.theme.colors.blue[700] : p.theme.colors.teal[700]) : 'none'};
`
const printPrice = (price: number) => {
  if (price < 1) return price.toFixed(4)
  if (price < 10) return price.toFixed(3)
  if (price < 100) return price.toFixed(2)
  return price.toFixed(0)
}

const Y: React.FC<{
  yAxisPadding: string
  xAxisPadding: string
  labels: { price: number; placement: number }[]
  current?: { price: number; placement: number }
  levels?: { price: number; placement: number }[]
}> = ({ xAxisPadding, yAxisPadding, labels, current, levels = [] }) => {
  return (
    <YAxis xAxisPadding={xAxisPadding} yAxisPadding={yAxisPadding}>
      {labels.map((v, i) => (
        <YLabel key={'ylabel-label' + i} placement={v.placement}>
          {printPrice(v.price)}
        </YLabel>
      ))}
      {levels.map((v, i) => (
        <YLabel key={'level-' + i} placement={v.placement} type="level">
          {printPrice(v.price)}
        </YLabel>
      ))}
      {current && (
        <YLabel key={'ylabel-current'} placement={current.placement} type="current">
          {printPrice(current.price)}
        </YLabel>
      )}
    </YAxis>
  )
}

export const Axises = {
  X,
  Y,
}
