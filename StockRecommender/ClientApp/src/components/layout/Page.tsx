import styled, { keyframes, css } from 'styled-components'
import React, { Suspense } from 'react'
import { Loader } from '../Loader'

const maxWidth = '100vw'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  min-height: 100vh;
  background: ${(p) => p.theme.colors.backgrounds.main};
  color: ${(p) => p.theme.colors.text.onDark};

  @media print {
    min-height: 0;
    background: none;
  }
`

// we use box-sizing: content-box here so that we may avoid having the left/right padding take up part of the well-defined max-width
const Content = styled.div<{ align?: 'start' | 'end' | 'center' }>`
  display: flex;
  flex-direction: column;
  align-items: ${(p) => (p.align ? p.align : 'start')};
  box-sizing: content-box;
  width: 100%;
  max-width: ${maxWidth};
  padding-left: 1vw;
  padding-right: 1vw;
  margin: 0px auto;
  padding-bottom: 5vh;

  @media print {
    max-width: none;
    padding: 0;
  }
`

const ContentTop = styled(Content)`
  padding-bottom: 0;
`

const pulseAnimation = (color: string) => keyframes`
  0% {
    box-shadow: inset 0 0 0 0 ${color};
  }
  70% {
      box-shadow: inset 0 0 0 10vmin rgba(0, 0, 0, 0);
  }
  100% {
      box-shadow: inset 0 0 0 0 rgba(0, 0, 0, 0);
  }
`

const PageContent = styled.div<{ pulse?: boolean }>`
  display: flex;
  flex: 1;
  flex-direction: column;

  ${(p) =>
    p.pulse === true &&
    css`
      animation: ${pulseAnimation(p.theme.colors.deepOrange[500])} 2s ease-in-out 0.5s infinite;
    `}
`

const Row = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
`

const HeaderRow = styled.div`
  display: flex;
  flex-direction: row;
  background: ${(p) => p.theme.colors.backgrounds.panel};
  padding: 1rem;
  border-bottom: 1px solid rgba(255, 255, 255, 0.1);
`

export const PageHeadWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`

export const PageHead: React.FC = ({ children }) => (
  <HeaderRow>
    <ContentTop>{children}</ContentTop>
  </HeaderRow>
)

export const PageBody: React.FC<{ align?: 'start' | 'end' | 'center' }> = ({ children, align = 'start' }) => (
  <Row>
    <Content align={align}>{children}</Content>
  </Row>
)

export const Page: React.FC = ({ children }) => (
  <>
    <Container>
      <Suspense fallback={Loader}>
        <PageContent>{children}</PageContent>
      </Suspense>
    </Container>
  </>
)
