import styled from 'styled-components'

const Start = styled.div`
  display: flex;
  justify-content: start;
`
const End = styled.div`
  display: flex;
  justify-content: end;
`
const Center = styled.div`
  display: flex;
  justify-content: center;
`

export const ContentAlign = {
  Start,
  End,
  Center,
}
