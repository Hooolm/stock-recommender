import styled, { css } from 'styled-components'

const base = css`
  font-family: ${(p) => p.theme.fonts.nunitoSans};
  margin: 0;
`

export const S1 = styled.h6`
  ${base}
  font-size: 16px;
  line-height: 0.9;
`
S1.displayName = 'S1'

export const S2 = styled.h6`
  ${base}
  font-size: 14px;
  line-height: 0.95;
`
S2.displayName = 'S2'
