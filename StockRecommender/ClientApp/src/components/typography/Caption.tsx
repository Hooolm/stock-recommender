import styled, { css } from 'styled-components'

const base = css`
  font-family: ${(p) => p.theme.fonts.nunitoSans};
  margin: 0;
`

export const Caption = styled.p`
  ${base}
  font-size: 12px;
  letter-spacing: 0.15rem;
  font-weight: 700;
`
Caption.displayName = 'Caption'
