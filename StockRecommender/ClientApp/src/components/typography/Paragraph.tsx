import styled, { css } from 'styled-components'

const base = css`
  font-family: ${(p) => p.theme.fonts.nunitoSans};
  margin: 0;
`

export const P1 = styled.p`
  ${base}
  font-size: 16px;
`
P1.displayName = 'P1'

export const P2 = styled.p`
  ${base}
  font-size: 14px;
`
P2.displayName = 'P2'

export const P3 = styled.p`
  ${base}
  font-size: 11px;
`
P3.displayName = 'P3'
