import React from 'react'
import styled from 'styled-components'
const ReactMarkdown = require('react-markdown/with-html')

const StyleWrapper = styled.div`
  font-family: ${(p) => p.theme.fonts.nunitoSans};
  ol,
  ul {
    padding-inline-start: 24px;
  }
`
export const Markdown: React.FC<{ label: string } & React.ComponentProps<typeof ReactMarkdown>> = ({
  label,
  ...props
}) => (
  <StyleWrapper>
    <ReactMarkdown {...props} />
  </StyleWrapper>
)
