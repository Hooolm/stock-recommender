import styled from 'styled-components'

const Start = styled.div`
  text-align: start;
`
const End = styled.div`
  text-align: end;
`
const Center = styled.div`
  text-align: center;
`

export const TextAlign = {
  Start,
  End,
  Center,
}
