import styled, { css } from 'styled-components'

const base = css`
  font-family: ${(p) => p.theme.fonts.nunitoSans};
  margin: 0;
`

export const H1 = styled.h1`
  ${base}
  font-size: 96px;
`
H1.displayName = 'H1'

export const H2 = styled.h2`
  ${base}
  font-size: 60px;
`
H2.displayName = 'H2'

export const H3 = styled.h3`
  ${base}
  font-size: 48px;
`
H3.displayName = 'H3'

export const H4 = styled.h4`
  ${base}
  font-size: 34px;
`
H4.displayName = 'H4'

export const H5 = styled.h5`
  ${base}
  font-size: 24px;
`
H5.displayName = 'H5'

export const H6 = styled.h6`
  ${base}
  font-size: 20px;
`
H6.displayName = 'H6'
