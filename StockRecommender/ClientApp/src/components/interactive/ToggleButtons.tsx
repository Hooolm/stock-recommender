import React from 'react'
import ToggleButton from '@material-ui/lab/ToggleButton'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'
import { S1 } from '../typography/Subtitle'

export const ToggleButtons: React.FC<{
  options: string[]
  value: string
  onChange: (value: string) => void
}> = ({ options, onChange, value }) => {
  return (
    <ToggleButtonGroup value={value} exclusive onChange={(e, v: string) => onChange(v)}>
      {options.map((opt) => (
        <ToggleButton key={opt} value={opt}>
          <S1>{opt}</S1>
        </ToggleButton>
      ))}
    </ToggleButtonGroup>
  )
}
