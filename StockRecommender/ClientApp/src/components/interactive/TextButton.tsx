import styled from 'styled-components'

export const TextButton = styled.button`
  text-decoration: none;
  border: none;
  background: none;
  cursor: pointer;
  color: ${(p) => p.theme.colors.deepOrange[500]};
  transition: 100ms color;
  outline: none;
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 0;

  &:hover,
  &:active {
    color: ${(p) => p.theme.colors.deepOrange[900]};
  }
`
