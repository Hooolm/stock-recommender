import styled from 'styled-components'

export const Anchor = styled.a`
  text-decoration: none;
  color: ${(p) => p.theme.colors.white};
  text-decoration: underline;
  transition: 100ms color;

  &:hover,
  &:focus,
  &:active {
    color: ${(p) => p.theme.colors.deepOrange[900]};
  }
`
