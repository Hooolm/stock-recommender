import styled from 'styled-components'
import React, { useCallback, useEffect, useState } from 'react'
import { Box, debounce } from '@material-ui/core'
import { ClickAwayListener } from '@material-ui/core'

const Container = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  margin: 1rem 0;
`

const SuggestionContainer = styled.div`
  position: absolute;
  top: 39px;
  background: ${(p) => p.theme.colors.grey[900]};
  width: 100%;
  z-index: 1;
`

const Suggestion = styled.p<{ selected?: boolean }>`
  font-size: 1rem;
  margin: 0;
  font-family: ${(p) => p.theme.fonts.nunitoSans};
  padding: 0.5rem;
  background: ${(p) => (p.selected ? p.theme.colors.deepOrange[600] : 'none')};
  :hover {
    background: ${(p) => p.theme.colors.deepOrange[600]};
  }

  &:not(:first-child) {
    border-top: 1px solid rgba(0, 0, 0, 0.3);
  }
  &.selected {
  }
  display: flex;
  justify-content: space-between;
`

const Input = styled.input`
  background: ${(p) => p.theme.colors.grey[900]};
  border: none;
  margin: 0;
  font-size: 1rem;
  font-family: ${(p) => p.theme.fonts.nunitoSans};
  padding: 0.5rem;
  width: 100%;
  color: #fff;
  outline: none;
  font-weight: 700;
`
export type OptionType = {}

export const SearchField: React.FC<{
  onSelect: (select: string) => void
  options?: { value: string; alternative: string; category: string }[]
  onSearch: (search: string) => void
}> = ({ onSelect, options: opt = [], onSearch }) => {
  const [selected, setSelected] = useState(undefined as number | undefined)
  const [inputValue, setInputValue] = useState('')
  const [cleared, setCleared] = useState(false)

  const options = cleared ? [] : opt

  useEffect(() => {
    setSelected(undefined)
  }, [options])

  const bouncedSearch = useCallback(debounce(onSearch, 500), [onSearch])

  useEffect(() => {
    if (inputValue === '') {
      setCleared(true)
      return
    }
    setCleared(false)
    bouncedSearch(inputValue)
  }, [inputValue])

  const increment = useCallback(() => {
    if (selected === undefined || selected === options.length - 1) setSelected(0)
    else setSelected(selected + 1)
  }, [options, selected])

  const decrement = useCallback(() => {
    if (selected === undefined || selected === 0) setSelected(options.length - 1)
    else setSelected(selected - 1)
  }, [options, selected])

  return (
    <ClickAwayListener onClickAway={() => setInputValue('')}>
      <Container>
        <Input
          placeholder="Stonk?"
          value={inputValue}
          onChange={(x) => setInputValue(x.target.value)}
          onKeyDown={(x) => {
            if (x.key === 'ArrowDown') increment()
            if (x.key === 'ArrowUp') decrement()
            if (x.key === 'Enter' && selected !== undefined) {
              setInputValue('')
              onSelect(options[selected].value)
            }
          }}
        />
        <SuggestionContainer>
          {options.map((x, i) => (
            <Suggestion
              key={x.value}
              selected={i === selected}
              onClick={() => {
                onSelect(options[i].value)
              }}
            >
              <Box display="flex">
                <Box minWidth="100px">{x.value}</Box>
                <Box color={'rgba(255,255,255,0.4)'}>{x.alternative}</Box>
              </Box>
              <Box>{x.category}</Box>
            </Suggestion>
          ))}
        </SuggestionContainer>
      </Container>
    </ClickAwayListener>
  )
}
