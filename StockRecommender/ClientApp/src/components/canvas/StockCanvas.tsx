import Color from 'color'
import { rgba } from 'polished'
import React, { useEffect, useRef } from 'react'
import { theme } from 'src/theme'
import { getEMAPoints, linearRegression, Point, Trend } from 'src/trends'
import { roundToDeciamls } from 'src/utils'

import styled from 'styled-components'
import { Loader } from '../Loader'

const Graph = { width: 1500, height: 500, heightPadding: 100 }

const CanvasWrapper = styled.div`
  width: 100%;
  align-self: center;
  justify-self: center;
  background: black;
  border: 1px solid rgba(255, 255, 255, 0.1);
  display: grid;
`
const LoaderWrapper = styled.div`
  grid-column: 1;
  grid-row: 1;
  top: 0;
  width: 100%;
  height: 100%;
`
const Canvas = styled.canvas`
  width: 100%;
  grid-column: 1;
  grid-row: 1;
`

const getDrawingContext = (canvas: HTMLCanvasElement | null) => {
  return canvas?.getContext('2d')
}

const clearCanvas = (canvas: HTMLCanvasElement | null) => {
  const ctx = canvas?.getContext('2d')
  if (!canvas || !ctx) return // no canvas skip

  ctx.clearRect(0, 0, canvas.width, canvas.height)
}

export const StockCanvas: React.FC<{
  quotes: Point[]
  windowSize: number
  getTrends: (points: Point[], windowSize: number) => Trend[]
  ema?: boolean
}> = ({ quotes, windowSize, ema = false, getTrends }) => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null)
  const canvas = canvasRef?.current
  const loading = quotes.length === 0
  const min = Math.min(...quotes.map((x) => x.y))
  const max = Math.max(...quotes.map((x) => x.y))

  const emaRanges = [
    { range: 5, color: theme.colors.teal[50] },
    { range: 10, color: theme.colors.teal[100] },
    { range: 15, color: theme.colors.teal[200] },
    { range: 20, color: theme.colors.teal[300] },
    { range: 25, color: theme.colors.teal[400] },
    { range: 30, color: theme.colors.teal[500] },
  ]

  const width = Graph.width / (quotes.length - 1)
  const getRelativeStroke = (w: number) => (w * Graph.width) / 1000

  useEffect(() => {
    if (quotes.length === 0 && canvas !== null) return
    const ctx = getDrawingContext(canvas)
    if (!canvas || !ctx) return // no canvas skip

    clearCanvas(canvas)

    const getX = (step: number) => step * width
    const getY = (price: number) =>
      Graph.height - ((price - min) / (max - min)) * Graph.height + Graph.heightPadding / 2

    const drawLine = (points: Point[], color: string) => {
      ctx.beginPath()
      ctx.lineWidth = getRelativeStroke(1)
      ctx.lineJoin = 'bevel'
      ctx.strokeStyle = color
      points.forEach((p) => {
        ctx.lineTo(getX(p.x), getY(p.y))
      })
      ctx.stroke()
      ctx.closePath()
    }

    const drawIntervals = (points: Point[], color: string) => {
      points.forEach((p) => {
        ctx.beginPath()
        ctx.lineWidth = getRelativeStroke(1)
        ctx.strokeStyle = color
        ctx.lineTo(getX(p.x), 0)
        ctx.lineTo(getX(p.x), Graph.height + Graph.heightPadding)
        ctx.stroke()
        ctx.closePath()
      })
    }

    const drawPointIndeicator = (point: Point, color: string) => {
      ctx.beginPath()
      ctx.lineWidth = getRelativeStroke(1)
      ctx.strokeStyle = color
      ctx.arc(getX(point.x), getY(point.y), getRelativeStroke(3), 0, getRelativeStroke(2) * Math.PI)
      ctx.stroke()
      ctx.closePath()
    }

    const drawTrendline = (points: Point[], color: string, fromX: number, toX: number) => {
      const regline = linearRegression(points)
      ctx.beginPath()
      ctx.lineTo(getX(fromX), getY(fromX * regline.m + regline.b))
      ctx.lineTo(getX(toX), getY(toX * regline.m + regline.b))
      ctx.strokeStyle = color
      ctx.stroke()
      ctx.closePath()

      ctx.beginPath()
      ctx.moveTo(getX(fromX), 0)
      ctx.lineTo(getX(fromX), Graph.height + Graph.heightPadding)
      ctx.moveTo(getX(toX), 0)
      ctx.lineTo(getX(toX), Graph.height + Graph.heightPadding)
      ctx.strokeStyle = rgba(255, 255, 255, 0.025)
      ctx.stroke()
      ctx.closePath()
    }

    const drawTrendChannel = (
      upperPoints: Point[],
      lowerPoints: Point[],
      color: string,
      fromX: number,
      toX: number
    ) => {
      const upperReg = linearRegression(upperPoints)
      const lowerReg = linearRegression(lowerPoints)
      ctx.beginPath()
      ctx.lineTo(getX(fromX), getY(fromX * upperReg.m + upperReg.b))
      ctx.lineTo(getX(toX), getY(toX * upperReg.m + upperReg.b))
      ctx.lineTo(getX(toX), getY(toX * lowerReg.m + lowerReg.b))
      ctx.lineTo(getX(fromX), getY(fromX * lowerReg.m + lowerReg.b))
      ctx.fillStyle = Color(color).alpha(0.3).toString()
      ctx.fill()
      ctx.closePath()

      ctx.beginPath()
      ctx.lineTo(getX(fromX), getY(fromX * upperReg.m + upperReg.b))
      ctx.lineTo(getX(toX), getY(toX * upperReg.m + upperReg.b))
      ctx.moveTo(getX(toX), getY(toX * lowerReg.m + lowerReg.b))
      ctx.lineTo(getX(fromX), getY(fromX * lowerReg.m + lowerReg.b))
      ctx.strokeStyle = Color(color).alpha(0.8).toString()
      ctx.stroke()
      ctx.closePath()
    }

    // Price line
    drawIntervals(quotes, theme.colors.grey[1000])
    drawLine(quotes, theme.colors.grey[500])

    const trends = getTrends(quotes, windowSize)

    if (ema) {
      emaRanges.forEach((ema) => {
        drawLine(getEMAPoints(quotes, ema.range), ema.color)
      })
    }

    trends.forEach((t) => {
      const firstPoint = t.points[0]
      const lastPoint = t.points[t.points.length - 1]

      const trendPoints = [...t.lowerBound.trendPoints, ...t.upperBound.trendPoints]

      trendPoints.forEach((point) => {
        drawPointIndeicator(point, Color(theme.colors.teal[900]).alpha(0.9).toString())
      })

      drawTrendChannel(
        t.upperBound.trendPoints,
        t.lowerBound.trendPoints,
        theme.colors.teal[900],
        firstPoint.x,
        lastPoint.x
      )

      // Score
      const midlePoint = t.points[Math.floor(t.points.length / 2)]
      const textW = 45
      ctx.beginPath()
      ctx.fillStyle = theme.colors.grey[900]
      ctx.font = '15px' + theme.fonts.nunitoSans
      ctx.arc(getX(midlePoint.x), getY(min), textW / 2, 0, 2 * Math.PI, false)
      ctx.fill()
      ctx.beginPath()
      ctx.textAlign = 'center'
      ctx.fillStyle = 'white'
      ctx.fillText(roundToDeciamls(t.score, 2) + '', getX(midlePoint.x), getY(min) + 6)
      ctx.fill()
    })
  }, [quotes, windowSize, canvas, ema, emaRanges, max, min, width, getTrends])

  return (
    <>
      <CanvasWrapper>
        <LoaderWrapper>{loading && <Loader />}</LoaderWrapper>
        <Canvas ref={canvasRef} width={Graph.width} height={Graph.heightPadding + Graph.height} />
      </CanvasWrapper>
    </>
  )
}
