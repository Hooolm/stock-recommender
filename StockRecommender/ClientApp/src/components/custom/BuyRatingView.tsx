import React, { useState, useEffect } from 'react'
import { TechnicalApi, BuyRating } from 'src/api'
import { Indicator } from './Indicator'

type BuyRatingProps = {
  symbol: string
}

export const BuyRatingView: React.FC<BuyRatingProps> = ({ symbol }) => {
  const [buyRating, setBuyRating] = useState<BuyRating | undefined>(undefined)

  useEffect(() => {
    TechnicalApi.getBuyRating(symbol).then((x) => {
      setBuyRating(x as BuyRating)
    })
  }, [symbol])

  return (
    <div>
      {buyRating && (
        <>
          <Indicator bgColor={buyRating.beatingAverages ? 'green' : 'red'} />
          <Indicator bgColor={buyRating.increasingVolume ? 'green' : 'red'} />
          <Indicator bgColor={buyRating.isGreenCandle ? 'green' : 'red'} />
          <Indicator bgColor={buyRating.upwardMAs ? 'green' : 'red'} />
        </>
      )}
    </div>
  )
}
