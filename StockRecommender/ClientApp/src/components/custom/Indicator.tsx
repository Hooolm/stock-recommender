import styled from 'styled-components'

export const Indicator = styled.div<{ bgColor: string }>`
  width: 10px;
  height: 10px;
  background: ${(p) => p.bgColor};
  border-radius: 20px;
`
