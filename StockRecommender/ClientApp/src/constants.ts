export const apiUrlPrefix =
  !process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? 'https://localhost:44317/api' : '/api'
