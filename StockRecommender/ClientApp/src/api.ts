import { apiUrlPrefix } from './constants'

//#region Yahoo
export type QouteInterval = '1m' | '5m' | '15m' | '30m' | '1h' | '1d' | '5d' | '1wk' | '1mo' | '3mo'

const yahooEndpoint = apiUrlPrefix + '/yahoo'

export type Quoute = {
  timestamp: Date
  close?: number
  adjustedClose?: number
  high?: number
  low?: number
  open?: number
  volume?: number
  isHammer: boolean
}

export type TimeSeries = {
  interval: QouteInterval
  symbol: string
  series: Quoute[]
  isGreenCandle: boolean
  beatingAverages: boolean
  increasingVolume: boolean
  upwardMAs: boolean
}

export type BuyRating = {
  beatingAverages: boolean
  increasingVolume: boolean
  isGreenCandle: boolean
  upwardMAs: boolean
  interval: string
  from: Date
  to: Date
  symbol: string
}

const getQoutes = (symbol: string, interval: string, from?: Date, to?: Date) => {
  var url = yahooEndpoint + `/quotes?symbol=${symbol}&interval=${interval}`
  if (from) {
    url += `&from=${from.toISOString()}`
  }
  if (to) {
    url += `&to=${to.toISOString()}`
  }
  return fetch(url)
    .then((response) => response.json())
    .then((x) => {
      return {
        ...x,
        series: x.series.map((e: any) => {
          return { ...e, timestamp: new Date(Date.parse(e.timestamp)) }
        }),
      } as TimeSeries
    })
}

const searchSymbol = (query: string) => {
  var url = yahooEndpoint + `/search?query=${query}`
  return fetch(url)
    .then((response) => response.json())
    .then((data) => {
      const symbols = data.quotes?.map((x: any) => ({
        value: x.symbol,
        alternative: x.shortname,
        category: x.typeDisp,
      }))

      if (symbols) return symbols as { value: string; alternative: string; category: string }[]
      return [] as { value: string; alternative: string; category: string }[]
    })
}

export type Price = {
  price: string
  change: string
  changePctStr: string
  changePct: number
  sentiment: 'good' | 'bad' | 'neutral'
  source: 'regular' | 'pre' | 'post' | undefined
}
export type Summary = {
  symbol: string
  other?: Price
  regular: Price
  marketState: 'regular' | 'pre' | 'post' | undefined
}

const getSentiment = (change: number) => {
  if (change > 0.0001) return 'good'
  if (change < -0.0001) return 'bad'
  return 'neutral'
}

const getMarketState = (state: string) => {
  if (!state) return undefined
  if (state === 'PRE') return 'pre'
  if (state === 'POST') return 'pre'
  if (state === 'REGULAR') return 'regular'
}

const getPriceSummary = (symbol: string) => {
  var url = yahooEndpoint + `/summary?symbol=${symbol}`
  return fetch(url)
    .then((response) => response.json())
    .then((data) => {
      const priceObj = data?.quoteSummary?.result[0]?.price
      if (priceObj === undefined) {
        return undefined
      }

      return {
        symbol: symbol,
        other: priceObj.preMarketPrice?.fmt
          ? {
              price: priceObj.preMarketPrice.fmt,
              change: priceObj.preMarketChange.fmt,
              changePctStr: priceObj.preMarketChangePercent.fmt,
              changePct: priceObj.preMarketChangePercent.raw,
              sentiment: getSentiment(priceObj.preMarketChangePercent.raw),
              source: 'pre',
            }
          : priceObj.postMarketPrice?.fmt
          ? {
              price: priceObj.postMarketPrice.fmt,
              change: priceObj.postMarketChange.fmt,
              changePctStr: priceObj.postMarketChangePercent.fmt,
              changePct: priceObj.postMarketChangePercent.raw,
              sentiment: getSentiment(priceObj.postMarketChangePercent.raw),
              source: 'post',
            }
          : undefined,
        regular: {
          price: priceObj.regularMarketPrice.fmt,
          change: priceObj.regularMarketChange.fmt,
          changePctStr: priceObj.regularMarketChangePercent.fmt,
          changePct: priceObj.regularMarketChangePercent.raw,
          sentiment: getSentiment(priceObj.regularMarketChangePercent.raw),
          source: 'regular',
        },
        marketState: getMarketState(priceObj.marketState),
      } as Summary
    })
}

export const YahooApi = {
  getQoutes,
  searchSymbol,
  getPriceSummary,
}
//#endregion

//#region Technical
const technicalEndpoint = apiUrlPrefix + '/technical'

export type TouchRankType = {
  levels: { price: number; score: number }[]
  signal: 'buy' | 'sell' | 'hold'
}

const getTouchRanks = (symbol: string, interval: string, from?: Date, to?: Date) => {
  var url = technicalEndpoint + `/touch-ranks?symbol=${symbol}&interval=${interval}`
  if (from) {
    url += `&from=${from.toISOString()}`
  }
  if (to) {
    url += `&to=${to.toISOString()}`
  }
  return fetch(url)
    .then((response) => response.json())
    .then((x) => {
      return x as TouchRankType
    })
}

export type CandlePatternType = {
  type: string
  sentiment: 'bearish' | 'bullish' | 'neutral'
  from: Date
  to: Date
}

const getCandlePatterns = (symbol: string, interval: string, from?: Date, to?: Date) => {
  var url = technicalEndpoint + `/candle-patterns?symbol=${symbol}&interval=${interval}`
  if (from) {
    url += `&from=${from.toISOString()}`
  }
  if (to) {
    url += `&to=${to.toISOString()}`
  }
  return fetch(url)
    .then((response) => response.json())
    .then((x) => {
      return x.map((cp: any) => {
        return { ...cp, from: new Date(Date.parse(cp.from)), to: new Date(Date.parse(cp.to)) }
      }) as CandlePatternType[]
    })
}

export type TrendChannel = {
  members: TrendMemeber[]
  highs: TrendMemeber[]
  lows: TrendMemeber[]
}

export type TrendMemeber = Quoute & {
  type: 'high' | 'low'
}

const getTrendChannel = (symbol: string, interval: string, from?: Date, to?: Date) => {
  var url = technicalEndpoint + `/trend-channel?symbol=${symbol}&interval=${interval}`
  if (from) {
    url += `&from=${from.toISOString()}`
  }
  if (to) {
    url += `&to=${to.toISOString()}`
  }
  return fetch(url)
    .then((response) => response.json())
    .then((x) => {
      return {
        ...x,
        members: x.members.map((e: any) => {
          return { ...e, timestamp: new Date(Date.parse(e.timestamp)) }
        }),
        highs: x.highs.map((e: any) => {
          return { ...e, timestamp: new Date(Date.parse(e.timestamp)) }
        }),
        lows: x.lows.map((e: any) => {
          return { ...e, timestamp: new Date(Date.parse(e.timestamp)) }
        }),
      } as TrendChannel
    })
}

export type EmaEntry = {
  timestamp: Date
  value: number
}

export type EmaBand = {
  entries: EmaEntry[]
  symbol: string
  lookBack: number
}

const getEmaBand = (symbol: string, interval: string, lookBack?: number, from?: Date, to?: Date) => {
  var url = technicalEndpoint + `/ema-band?symbol=${symbol}&interval=${interval}`
  if (from) {
    url += `&from=${from.toISOString()}`
  }
  if (to) {
    url += `&to=${to.toISOString()}`
  }
  if (lookBack) {
    url += `&lookBack=${lookBack}`
  }
  return fetch(url)
    .then((response) => response.json())
    .then((x) => {
      return {
        ...x,
        entries: x.entries.map((e: any) => {
          return { ...e, timestamp: new Date(Date.parse(e.timestamp)) }
        }),
      } as EmaBand
    })
}

const getBuyRating = (symbol: string) => {
  var url = technicalEndpoint + `/buy-rating?symbol=${symbol}`

  return fetch(url)
    .then((response) => response.json())
    .then((x) => {
      return x as BuyRating
    })
}

export const TechnicalApi = {
  getTouchRanks,
  getCandlePatterns,
  getTrendChannel,
  getEmaBand,
  getBuyRating,
}

//#endregion
