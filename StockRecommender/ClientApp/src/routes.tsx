import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Page, PageBody, PageHead, PageHeadWrapper } from './components/layout/Page'
import { Anchor } from './components/navigation/Anchor'
import { S1 } from './components/typography/Subtitle'

const Details = React.lazy(() => import('./pages/Details').then((p) => ({ default: p.Details })))
const Cowboy = React.lazy(() => import('./pages/Cowboy').then((p) => ({ default: p.Cowboy })))
const Watchlist = React.lazy(() => import('./pages/Watchlist').then((p) => ({ default: p.Watchlist })))
const NotFound = React.lazy(() => import('./pages/NotFound').then((p) => ({ default: p.NotFound })))

export const routes = (
  <Page>
    <PageHead>
      <PageHeadWrapper>
        <Anchor href="/">
          <S1>Watchlist</S1>
        </Anchor>
      </PageHeadWrapper>
    </PageHead>
    <PageBody>
      <Switch>
        <Route path="/" exact component={Watchlist} />
        <Route path="/graph/:symbol" exact component={Details} />
        <Route path="/graph/" exact component={Details} />
        <Route path="/cowboy/" exact component={Cowboy} />
        {/* fall-through */}
        <Route component={NotFound} />
      </Switch>
    </PageBody>
  </Page>
)
