// import original module declarations
import 'styled-components'
import { theme } from '../theme'

type Theme = typeof theme

declare module 'styled-components' {
  // tslint:disable-next-line
  export interface DefaultTheme extends Theme {}
}
