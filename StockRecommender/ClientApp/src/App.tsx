import React from 'react'
import { ThemeProvider } from 'styled-components'
import { ThemeProvider as MUIThemeProvider } from '@material-ui/core/styles'
import { theme, muiTheme } from './theme'
import { BrowserRouter } from 'react-router-dom'
import { routes } from './routes'
import { Page } from './components/layout/Page'
import Helmet from 'react-helmet'

export const App = () => (
  <>
    <Helmet defaultTitle="Stock recommender" titleTemplate="%s | Stock recommender" />
    <BrowserRouter>
      <MUIThemeProvider theme={muiTheme}>
        <ThemeProvider theme={theme}>
          <Page>{routes}</Page>
        </ThemeProvider>
      </MUIThemeProvider>
    </BrowserRouter>
  </>
)
