export type RecipeIngredient = {
  name: string
  amount: string
  ingredientId: number
}

export type Recipe = {
  name: string
  id: number
  ingredients: RecipeIngredient[]
  procedure: string
}

export type ShoppingList = {
  id: string
  categories: Category[]
  extras: string[]
  lastEmptied: string
}

export type Category = {
  name: string
  items: Item[]
}

export type Item = {
  name: string
  amount: string
  id: number
}
