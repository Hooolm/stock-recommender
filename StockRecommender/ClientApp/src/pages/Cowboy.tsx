import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { TechnicalApi } from 'src/api'
import { Box, Button } from '@material-ui/core'
import { S1 } from 'src/components/typography/Subtitle'

export const Cowboy: React.FC<any> = (props) => {
  const query = new URLSearchParams(useLocation().search)
  const symbolsFromQuery = query.get('symbols')?.split(',') ?? []
  const [symbols, setsymbols] = useState([] as string[])
  const [hasCandlePatterns, setHasCandlePatterns] = useState([] as string[])
  const { push } = useHistory()

  useEffect(() => {
    const symbolsAsJson = localStorage.getItem('watchlist.symbols')
    if (symbolsAsJson) {
      setsymbols(JSON.parse(symbolsAsJson))
    }
  }, [])

  useEffect(() => {
    localStorage.setItem('watchlist.symbols', JSON.stringify(symbols))
  }, [symbols])

  const checkForCandlePatterns = () => {
    var to = new Date()
    var from = new Date(to)
    from.setDate(from.getDate() - 365)
    Promise.all(
      symbols.map((s) => {
        return TechnicalApi.getCandlePatterns(s, '1d', from, to).then((t) => ({
          symbol: s,
          hasPatterns: t.filter((x) => x.type === 'darkCloudCover' || x.type === 'piercingLine').length > 0,
        }))
      })
    ).then((res) => {
      const symbolsWithPatterns = res.filter((r) => r.hasPatterns === true).map((r) => r.symbol)
      setHasCandlePatterns(symbolsWithPatterns)
    })
  }

  return (
    <Box display="flex" flexDirection="column">
      <Button onClick={() => setsymbols(symbolsFromQuery)}>Import</Button>
      <Button onClick={() => push('?symbols=' + symbols.join(','))}>Export</Button>
      <Box>
        {hasCandlePatterns.length === 0 && (
          <Button onClick={() => checkForCandlePatterns()}>Check candle patterns</Button>
        )}
        {hasCandlePatterns.length > 0 && <S1>{hasCandlePatterns.join(', ')}</S1>}
      </Box>
    </Box>
  )
}
