import {
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton,
  TableSortLabel,
  Box,
} from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { YahooApi, Summary } from 'src/api'
import { SearchField } from 'src/components/SearchField'
import { P2, P3 } from 'src/components/typography/Paragraph'
import ClearRoundedIcon from '@material-ui/icons/ClearRounded'
import styled from 'styled-components'
import WbSunnyRoundedIcon from '@material-ui/icons/WbSunnyRounded'
import NightsStayRoundedIcon from '@material-ui/icons/NightsStayRounded'
import GavelRoundedIcon from '@material-ui/icons/GavelRounded'
import ReplayRoundedIcon from '@material-ui/icons/ReplayRounded'
import { theme } from 'src/theme'
import { useHistory } from 'react-router'
import { Caption } from 'src/components/typography/Caption'
import { BuyRatingView } from 'src/components/custom/BuyRatingView'

const DenseCell = styled(TableCell)`
  padding: 0.5rem 0 !important;
  line-height: 1 !important;
  display: flex;
  flex-direction: row;
`

const Centered = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

const Rotateable = styled(Centered)<{ rotating?: boolean }>`
  @keyframes rotation {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(-359deg);
    }
  }
  animation: ${(p) => p.rotating && `rotation 0.2s infinite cubic-bezier(0.2, 0.7, 0.8, 0.5);`};
`
const Clickable = styled.div`
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`

const ContentWrapper = styled.div<{ alignment?: 'start' | 'center' | 'end' }>`
  display: flex;
  flex-direction: row;
  justify-content: ${(p) => (p.alignment === 'end' ? 'flex-end' : p.alignment === 'center' ? 'center' : 'flex-start')};
  align-items: center;
`

const SentimentCell = styled(DenseCell)<{ sentiment: 'good' | 'bad' | 'neutral' }>`
  color: ${(p) =>
    p.sentiment === 'good'
      ? p.theme.colors.green[400]
      : p.sentiment === 'bad'
      ? p.theme.colors.red[400]
      : p.theme.colors.white} !important;
`

export const Watchlist: React.FC = () => {
  const [symbolSuggestions, setsymbolSuggestions] = useState(
    [] as { value: string; alternative: string; category: string }[]
  )
  const [updated, setUpdated] = useState(Date.now())
  const [loading, setLoading] = useState(false)
  const [symbols, setsymbols] = useState([] as string[])
  const [summaries, setSummaries] = useState([] as Summary[])
  const [sort, setSort] = useState({
    header: 'symbol' as 'symbol' | 'market' | 'other',
    direction: 'asc' as 'asc' | 'desc',
  })

  const { push } = useHistory()

  const handleSort = (header: 'symbol' | 'market' | 'other') => {
    const newSort = {
      header: header,
      direction: (header !== sort.header ? 'asc' : sort.direction === 'asc' ? 'desc' : 'asc') as 'asc' | 'desc',
    }
    setSort(newSort)
  }

  const sortedSummaries = summaries.sort((a, b) => {
    const first = sort.direction === 'asc' ? b : a
    const second = sort.direction === 'asc' ? a : b
    if (sort.header === 'market') {
      return (first.regular.changePct ?? 0) - (second.regular.changePct ?? 0)
    }

    if (sort.header === 'other') {
      return (first?.other?.changePct ?? 0) - (second?.other?.changePct ?? 0)
    }

    return ('' + second.symbol).localeCompare('' + first.symbol)
  })

  useEffect(() => {
    const symbolsAsJson = localStorage.getItem('watchlist.symbols')
    if (symbolsAsJson) {
      setsymbols(JSON.parse(symbolsAsJson))
    }

    const sortAsJson = localStorage.getItem('watchlist.sort')
    if (sortAsJson) {
      setSort(JSON.parse(sortAsJson))
    }
  }, [])

  const symbolExists = (symbol: string) => {
    if (!symbol) return false
    if (symbols.indexOf(symbol) !== -1) return true
    return false
  }

  const addsymbol = (symbol: string) => {
    if (symbolExists(symbol)) return
    setsymbols([...symbols, symbol])
  }

  const removesymbol = (symbol: string) => {
    if (!symbolExists(symbol)) return
    const index = symbols.indexOf(symbol)
    setsymbols([...symbols.slice(0, index), ...symbols.slice(index + 1, symbols.length - 1)])
  }

  useEffect(() => {
    localStorage.setItem('watchlist.symbols', JSON.stringify(symbols))
  }, [symbols])

  useEffect(() => {
    localStorage.setItem('watchlist.sort', JSON.stringify(sort))
  }, [sort])

  useEffect(() => {
    setLoading(true)
    Promise.all(symbols.map((symbol) => YahooApi.getPriceSummary(symbol)))
      .then((x) => {
        setSummaries(x.filter((x) => x !== undefined) as Summary[])
      })
      .then(() => setLoading(false))
  }, [symbols, updated])

  useEffect(() => {
    const id = setInterval(() => {
      if (symbols.length > 0) {
        setUpdated(Date.now())
      }
    }, 30000)
    return () => clearInterval(id)
  }, [symbols])

  return (
    <>
      <SearchField
        onSelect={(x) => {
          if (symbolExists(x)) {
            window.location.hash = x
          }
          addsymbol(x)
          setsymbolSuggestions([])
        }}
        onSearch={(x) => {
          YahooApi.searchSymbol(x).then((s) => setsymbolSuggestions(s))
        }}
        options={symbolSuggestions}
      />
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <DenseCell width="35px">
                <Clickable>
                  <Rotateable rotating={loading}>
                    <ReplayRoundedIcon
                      fontSize="small"
                      onClick={() => {
                        if (!loading) {
                          setUpdated(Date.now())
                        }
                      }}
                    />
                  </Rotateable>
                </Clickable>
              </DenseCell>
              <DenseCell width="30px">
                <TableSortLabel
                  active={sort.header === 'symbol'}
                  direction={sort.direction}
                  onClick={() => {
                    handleSort('symbol')
                  }}
                >
                  <Caption>SYMBOL</Caption>
                </TableSortLabel>
              </DenseCell>
              <DenseCell align="right">
                <TableSortLabel
                  active={sort.header === 'market'}
                  direction={sort.direction}
                  onClick={() => handleSort('market')}
                >
                  <Caption>MARKET</Caption>
                </TableSortLabel>
              </DenseCell>
              <DenseCell align="right">
                <TableSortLabel
                  active={sort.header === 'other'}
                  direction={sort.direction}
                  onClick={() => handleSort('other')}
                >
                  <Caption>PRE/POST</Caption>
                </TableSortLabel>
              </DenseCell>
              <DenseCell width="25px" align="left"></DenseCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sortedSummaries.map((summary) => {
              return (
                <TableRow key={summary.symbol} id={summary.symbol}>
                  <DenseCell width="35px">
                    <Centered>
                      {summary.marketState === 'pre' && (
                        <WbSunnyRoundedIcon htmlColor={theme.colors.amber[500]} fontSize="small" />
                      )}
                      {summary.marketState === 'post' && (
                        <NightsStayRoundedIcon htmlColor={theme.colors.blue[200]} fontSize="small" />
                      )}
                      {summary.marketState === 'regular' && (
                        <GavelRoundedIcon htmlColor={theme.colors.green[500]} fontSize="small" />
                      )}
                    </Centered>
                  </DenseCell>
                  <DenseCell component="th" align="left">
                    <Clickable onClick={() => push('/graph/' + summary.symbol)}>
                      <ContentWrapper alignment="start">
                        <Box>
                          <P2>
                            <span>{summary.symbol}</span>
                          </P2>
                        </Box>
                      </ContentWrapper>
                    </Clickable>
                  </DenseCell>
                  <SentimentCell sentiment={summary.regular.sentiment} align="right">
                    <P2>{summary.regular.changePctStr}</P2>
                    <P3>
                      {summary.regular.price} ({summary.regular.change})
                    </P3>
                  </SentimentCell>
                  <SentimentCell sentiment={summary.other ? summary.other.sentiment : 'neutral'} align="right">
                    {summary.marketState !== 'regular' && summary.other && (
                      <ContentWrapper alignment="end">
                        <Box>
                          <P2>{summary.other.changePctStr}</P2>
                          <P3>
                            {summary.other.price} ({summary.other.change})
                          </P3>
                        </Box>
                        <Box ml={1}>
                          <BuyRatingView symbol={summary.symbol} />
                        </Box>
                        {/* <Box ml={1}>
                          {summary.other.source === 'pre' && (
                            <WbSunnyRoundedIcon htmlColor={theme.colors.amber[500]} fontSize="small" />
                          )}
                          {summary.other.source === 'post' && (
                            <NightsStayRoundedIcon htmlColor={theme.colors.blue[200]} fontSize="small" />
                          )}
                        </Box> */}
                      </ContentWrapper>
                    )}
                  </SentimentCell>
                  <DenseCell>
                    <IconButton onClick={() => removesymbol(summary.symbol)} aria-label="delete" size="small">
                      <ClearRoundedIcon fontSize="small" />
                    </IconButton>
                  </DenseCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}
