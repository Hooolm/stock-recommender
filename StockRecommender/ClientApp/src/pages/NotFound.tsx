import React from 'react'
import styled from 'styled-components'
import { PageBody, Page } from 'src/components/layout/Page'
import { S1 } from 'src/components/typography/Subtitle'
import { H1 } from 'src/components/typography/Heading'
import Helmet from 'react-helmet'

const CenteredContent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-bottom: 15vh;
  text-align: center;
`

export const NotFound = () => (
  <Page>
    <Helmet>
      <title>404 - ikke fundet</title>
    </Helmet>
    <PageBody align={'center'}>
      <CenteredContent>
        <div>
          <H1>
            <span role="img" aria-label=":detective:">
              🕵️
            </span>
          </H1>
          <br />
          <br />
          <S1>Vi kunne desværre ikke finde den side til dig</S1>
        </div>
      </CenteredContent>
    </PageBody>
  </Page>
)
