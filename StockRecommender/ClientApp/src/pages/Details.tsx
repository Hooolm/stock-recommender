import { Box, Checkbox } from '@material-ui/core'
import React, { useEffect, useMemo, useState } from 'react'
import {
  YahooApi,
  QouteInterval,
  TimeSeries,
  TouchRankType,
  TechnicalApi,
  CandlePatternType,
  TrendChannel,
  EmaBand,
} from 'src/api'
import { Indicator } from 'src/components/custom/Indicator'
import { Graph } from 'src/components/graph/Graph'
import { ToggleButtons } from 'src/components/interactive/ToggleButtons'
import { Loader } from 'src/components/Loader'
import { Caption } from 'src/components/typography/Caption'
import { P2 } from 'src/components/typography/Paragraph'
import { S2 } from 'src/components/typography/Subtitle'
import { theme } from 'src/theme'
import styled from 'styled-components'

const DetailsContainer = styled(Box)`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
`

const SidePanel = styled(Box)`
  width: 200px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-right: 1rem;
  margin-top: 1rem;
  border-right: 1px solid rgba(255, 255, 255, 0.3);
  padding-right: 1rem;
`

const GraphWrapper = styled(Box)`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const EmaLegend = styled.div<{ bgColor: string }>`
  padding: 0.2rem;
  width: 1rem;
  height: 1rem;
  border-radius: 0.5rem;
  background: ${(p) => p.bgColor};
  display: flex;
  align-items: center;
  justify-content: center;
  p {
    text-shadow: 0px 0px 3px black;
  }
`

export const Details: React.FC<any> = (props) => {
  const useMemoProp = props?.match?.params?.symbol
  const symbol = useMemo(() => useMemoProp as string, [useMemoProp])
  const [timeSeries, setTimeSeries] = useState(undefined as TimeSeries | undefined)
  const [touchRank, setTouchRank] = useState(undefined as TouchRankType | undefined)
  const [shownTouchRank, setShownTouchRank] = useState(false)
  const [showCandlePatterns, setShowCandlePatterns] = useState(false)
  const [showTrendChannel, setShowTrendChannel] = useState(false)
  const [showEmaBands, setShowEmaBands] = useState(false)
  const [candlePatterns, setCandlePatterns] = useState([] as CandlePatternType[])
  const [trendChannel, setTrendChannel] = useState(undefined as TrendChannel | undefined)
  const [emaBands, setEmaBands] = useState([] as EmaBand[])
  const [loading, setLoading] = useState(false)

  const graphOptions = [
    { label: '1Y', daysBack: 365, interval: '1d' as QouteInterval },
    { label: '6M', daysBack: 183, interval: '1d' as QouteInterval },
    { label: '3M', daysBack: 91, interval: '1d' as QouteInterval },
    { label: '1M', daysBack: 31, interval: '1d' as QouteInterval },
  ]

  const [graphOption, setGraphOption] = useState(graphOptions[3])

  const handleOptionsChange = (label: string) => {
    const selected = graphOptions.filter((x) => x.label === label)
    if (selected?.length === 1) {
      setGraphOption(selected[0])
    } else {
      setGraphOption(graphOptions[0])
    }
  }

  useEffect(() => {
    setLoading(true)
    var to = new Date()
    var from = new Date(to)
    from.setDate(from.getDate() - graphOption.daysBack)
    YahooApi.getQoutes(symbol, graphOption.interval, from, to).then((x) => {
      setTimeSeries(x)
      setLoading(false)
    })
    TechnicalApi.getCandlePatterns(symbol, graphOption.interval, from, to).then((x) => {
      setCandlePatterns(x)
    })
    TechnicalApi.getTrendChannel(symbol, graphOption.interval, from, to).then((x) => {
      setTrendChannel(x)
    })

    TechnicalApi.getTouchRanks(symbol, graphOption.interval, from, to).then((x) => {
      setTouchRank(x)
    })

    setEmaBands([])
    TechnicalApi.getEmaBand(symbol, graphOption.interval, 2, from, to)
      .then((x) => {
        setEmaBands((prev) => [...prev, x])
      })
      .then(() => {
        TechnicalApi.getEmaBand(symbol, graphOption.interval, 3, from, to)
          .then((x) => {
            setEmaBands((prev) => [...prev, x])
          })
          .then(() => {
            TechnicalApi.getEmaBand(symbol, graphOption.interval, 5, from, to)
              .then((x) => {
                setEmaBands((prev) => [...prev, x])
              })
              .then(() => {
                TechnicalApi.getEmaBand(symbol, graphOption.interval, 8, from, to)
                  .then((x) => {
                    setEmaBands((prev) => [...prev, x])
                  })
                  .then(() => {
                    TechnicalApi.getEmaBand(symbol, graphOption.interval, 13, from, to)
                      .then((x) => {
                        setEmaBands((prev) => [...prev, x])
                      })
                      .then(() => {
                        TechnicalApi.getEmaBand(symbol, graphOption.interval, 21, from, to).then((x) => {
                          setEmaBands((prev) => [...prev, x])
                        })
                      })
                  })
              })
          })
      })
  }, [symbol, graphOption])

  return (
    <DetailsContainer>
      <SidePanel>
        <Box display="flex" flexDirection="column" justifyContent="space-between" width="100%">
          <Box display="flex" alignItems="center" justifyContent="space-between" mb={1}>
            <Caption>Green candle</Caption>
            <Indicator bgColor={timeSeries?.isGreenCandle ? 'green' : 'red'} />
          </Box>
          <Box display="flex" alignItems="center" justifyContent="space-between" mb={1}>
            <Caption>Beating Averages</Caption>
            <Indicator bgColor={timeSeries?.beatingAverages ? 'green' : 'red'} />
          </Box>
          <Box display="flex" alignItems="center" justifyContent="space-between" mb={1}>
            <Caption>Increasing Volume</Caption>
            <Indicator bgColor={timeSeries?.increasingVolume ? 'green' : 'red'} />
          </Box>
          <Box display="flex" alignItems="center" justifyContent="space-between" mb={1}>
            <Caption>Moving avgs</Caption>
            <Indicator bgColor={timeSeries?.upwardMAs ? 'green' : 'red'} />
          </Box>
          <Box display="flex" alignItems="center" justifyContent="space-between" mb={1}>
            <S2>Touch rank</S2>
            <Checkbox
              defaultChecked={false}
              onChange={(e) => {
                setShownTouchRank(e.target.checked)
              }}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          </Box>
          <Box display="flex" alignItems="center" justifyContent="space-between" mb={1}>
            <S2>Candle patterns</S2>
            <Checkbox
              defaultChecked={false}
              onChange={(e) => {
                setShowCandlePatterns(e.target.checked)
              }}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          </Box>
          <Box display="flex" alignItems="center" justifyContent="space-between" mb={1}>
            <S2>Highs and Lows</S2>
            <Checkbox
              defaultChecked={false}
              onChange={(e) => {
                setShowTrendChannel(e.target.checked)
              }}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          </Box>
          <Box display="flex" alignItems="center" justifyContent="space-between">
            <S2>Fibbonaci ema bands</S2>
            <Checkbox
              defaultChecked={false}
              onChange={(e) => {
                setShowEmaBands(e.target.checked)
              }}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          </Box>
          <Box display="flex" flexDirection="row" justifyContent="space-between" mr={1.2} mb={1}>
            <EmaLegend bgColor={theme.colors.red[900]}>
              <P2>2</P2>
            </EmaLegend>
            <EmaLegend bgColor={theme.colors.amber[900]}>
              <P2>3</P2>
            </EmaLegend>
            <EmaLegend bgColor={theme.colors.amber[400]}>
              <P2>5</P2>
            </EmaLegend>
            <EmaLegend bgColor={theme.colors.purple[400]}>
              <P2>8</P2>
            </EmaLegend>
            <EmaLegend bgColor={theme.colors.teal[800]}>
              <P2>13</P2>
            </EmaLegend>
            <EmaLegend bgColor={theme.colors.cyan[600]}>
              <P2>21</P2>
            </EmaLegend>
          </Box>
        </Box>
      </SidePanel>
      <GraphWrapper>
        <Box display="flex" alignItems="center" justifyContent="flex-start" width="100%">
          <Box m={1.5} ml={0}>
            <S2>{symbol.toUpperCase()}</S2>
          </Box>
          <Box m={1.5}>
            <ToggleButtons
              options={graphOptions.map((t) => t.label)}
              value={graphOption.label}
              onChange={(v) => handleOptionsChange(v)}
            />
          </Box>
        </Box>
        {!loading && timeSeries && trendChannel && (
          <Graph
            candlePatterns={showCandlePatterns ? candlePatterns : []}
            timeSeries={timeSeries}
            trendChannel={showTrendChannel ? trendChannel : undefined}
            touchRank={shownTouchRank ? touchRank : undefined}
            emaBands={showEmaBands ? emaBands : []}
          />
        )}
        {loading && <Loader />}
      </GraphWrapper>
    </DetailsContainer>
  )
}
