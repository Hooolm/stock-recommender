import { linearRegression as libLinear } from 'simple-statistics'


export const linearRegression = (points: Point[]) => {
  return libLinear(points.map((p) => [p.x, p.y]))
}

export type Point = {
  x: number
  y: number
}

const getEMANumbers = (a: number[], range: number) => a.reduce((p, n, i) => i ? p.concat(2 * n / (range + 1) + p[p.length - 1] * (range - 1) / (range + 1)) : p, [a[0]])
export const getEMAPoints = (points: Point[], range: number): Point[] => {
  return getEMANumbers(points.map(x => x.y), range).map((v, i) => ({ x: i, y: v }));
}

export type Trend = ReturnType<typeof findTrends>

export const isBlelow = (regPoints: Point[], points: Point[]) => {
  let regr = linearRegression(regPoints)

  return points.reduce((p, v) => {
    return p && v.y < regr.b + v.x * regr.m
  }, true)
}

export const isAbove = (regPoint: Point[], points: Point[]) => {
  let regr = linearRegression(regPoint)
  return points.reduce((p, v) => {
    return p && v.y > regr.b + v.x * regr.m
  }, true)
}

export const binaryReplace = (points: Point[], shouldReplace: (regPoints: Point[], points: Point[]) => boolean) => {
  const n = points.length
  const middle = Math.round(n / 2);
  const isEven = n % 2 === 0;
  let leftBest = { x: 0, y: 0 }
  let rightBest = { x: 0, y: 0 }
  let leftCandidate = { x: 0, y: 0 }
  let rightCandidate = { x: 0, y: 0 }

  for (var j = 0; j < 2 * n; j++) {

    const i = j % (middle - (isEven ? 0 : 1))
    const leftIndex = i
    const rightIndex = n - i - 1
    const previous = [...points.slice(0, leftIndex), ...points.slice(rightIndex, points.length - 1)]

    leftCandidate = points[leftIndex]
    rightCandidate = points[rightIndex]
    let leftTempBest = leftBest;
    let rightTempBest = rightBest;

    if (j === 0 || shouldReplace([leftBest, rightBest], [rightCandidate])) {
      rightTempBest = rightCandidate
    }

    if (j === 0 || shouldReplace([leftBest, rightBest], [leftCandidate])) {
      leftTempBest = leftCandidate
    }

    if (j <= n || !shouldReplace([leftTempBest, rightTempBest], previous)) {
      leftBest = leftTempBest;
      rightBest = rightTempBest;
    }
  }

  return [leftBest, rightBest]
}

export const verticalDistance = (point: Point, regLine: { m: number; b: number }) => {
  const lineYValue = regLine.m * point.x + regLine.b
  return Math.abs(point.y - lineYValue)
}
export const mseVerticalDistance = (points: Point[], regLine: { m: number; b: number }) => {
  let squaredError = 0

  points.forEach((p) => {
    squaredError += Math.pow(verticalDistance(p, regLine), 2)
  })

  return squaredError / points.length
}

export const findTrends = (points: Point[]) => {
  const upperPoints = binaryReplace(points, isBlelow)
  const lowerPoints = binaryReplace(points, isAbove)

  const upperTrend = linearRegression(upperPoints)
  const lowerTrend = linearRegression(lowerPoints)

  const score = (mseVerticalDistance(points, upperTrend) + mseVerticalDistance(points, lowerTrend)) / points.length
  return {
    upperBound: {
      trendPoints: upperPoints,
      trend: upperTrend,
    },
    lowerBound: {
      trendPoints: lowerPoints,
      trend: lowerTrend,
    },
    score: score,
    points: points,
  }
}

export const splitIntoTrends = (points: Point[], windowSize: number) => {
  let result = [] as Trend[]

  points.forEach((_, i) => {
    const j = points.length - i
    if (j % windowSize !== 0) return

    let slice = points.slice(i, i + windowSize + 1)
    result = [...result, findTrends(slice)]
  })

  return result
}

export const blindFit = (points: Point[], windowSize: number) => {
  const from = Math.max(0, points.length - windowSize);
  const to = points.length;
  return [findTrends(points.slice(from, to))];
}

export const findBestFitDaysBack = (points: Point[], options: { daysBack: number; shuffle: number }) => {
  let result = [] as Trend[]

  const shuffleMax = Math.max(Math.round(options.daysBack * Math.min(1, Math.max(0, options.shuffle))))
  const shuffleStep = Math.max(1, Math.ceil(shuffleMax / 3))
  for (var ls = 0; ls <= shuffleMax; ls += shuffleStep) {
    for (var rs = 0; rs <= shuffleMax; rs += shuffleStep) {
      const from = Math.max(points.length - options.daysBack - 1 - ls, 0);
      const to = Math.min(points.length - 1 - rs, points.length - 1)
      const slice = points.slice(from, to + 1)
      if (slice.length < 2) continue;
      result = [...result, findTrends(slice)]

    }

  }
  return [{ ...result.sort((a, b) => a.score - b.score).slice(0, 1)[0], points: points.slice(points.length - options.daysBack, points.length) }]
}
